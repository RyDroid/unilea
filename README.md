# UNIcamp Lasca Email Analyzer

The aim of this project is to create an analyzer of emails to find potential security threats.
It is [free/libre software](https://www.gnu.org/philosophy/free-sw.html).
Happy hacking and enjoy!

## Build and use

See [the file for GitLab CI](.gitlab-ci.yml) to know what software is needed.
Only free/libre software available in Debian (at least version 9) is required.
However, there is an exception in Python with [IMAPClient](https://bitbucket.org/mjs0/imapclient/), but it is free/libre.
There is an other one in Java with [a library to use VirusTotal Public API](https://github.com/kdkanishka/Virustotal-Public-API-V2.0-Client), but again it is free/libre.
The Apache Commons library for CSV in Java might be in your distribution in version "0.1" but it is too old, it is the case at least for Debian 7, Debian 8 and Trisquel 7.

Then you need to create the database and tables in it.
There are [PL/pgSQL scripts for that](src/sql/).

Some files have to be compiled, you can use `ant` or `make`.
See rules starting with "run-" to execute most scripts.

## Modify

You can import Java code in your favorite EDI if it can import a build of Apache Ant (like Eclipse).
Python and SQL code were written with [GNU Emacs](https://www.gnu.org/software/emacs/) and his auto-indentation feature.

## Get newer versions

You may find a new version with [the repository rydroid/unilea on GitLab.com](https://gitlab.com/RyDroid/unilea).
You may also ask to [LASCA](http://lasca.ic.unicamp.br/), a lab at Institute of Computing of UNICAMP (in Campinas, a city of Brazil).

## Documentation

There is no documentation of the program itself or manpages.
Nicola Spanti wrote a document in english and french that explains how and why it was built like that.
He may give it to you, you can contact him by email or XMPP.
You can also ask to LASCA that have also the 2 reports.
