#!/bin/sh

# Copying and distribution of this file, with or without modification,
# are permitted in any medium without royalty provided this notice is
# preserved.  This file is offered as-is, without any warranty.

# Replace the values to fit your configuration
host=localhost
dbname=unilea
schema=public
username=postgres
userpass=postgres
schema_spy_jar=schema-spy.jar

# You need (Java Runtume Environment) and SchemaSpy.
# http://openjdk.java.net/
# http://schemaspy.sourceforge.net/
if test -e "$schema_spy_jar"
then
    java -jar \
	 schema-spy.jar -t pgsql \
	 -host "$host" -db "$dbname" -s "$schema" \
	 -u "$username" -p "$userpass" -o schema \
	 -dp postgresql-jdbc.jar
fi

# http://autodoc.projects.pgfoundry.org/
# http://pgfoundry.org/projects/autodoc
# https://packages.debian.org/jessie/postgresql-autodoc
if test $(command -v postgresql_autodoc)
then
    postgresql_autodoc \
	-h "$host" -d "$dbname" -s "$schema" \
	-u "$username" "--password=$userpass"
fi
