/*
 * Copyright 2016  Nicola Spanti  <dev@nicola-spanti.info>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * https://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package br.unicamp.ic.lasca.unilea.file;


import org.junit.Test;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertFalse;

import br.unicamp.ic.lasca.unilea.file.FileDb;


public final class FileDbTests
{
	@Test
	public void hasValidId()
	{
		final FileDb file1 = new FileDb(+   1, "file.txt", "");
		assertTrue(file1.hasValidId());
		
		final FileDb file2 = new FileDb(+1000, "file.txt", "");
		assertTrue(file2.hasValidId());
	}
	
	@Test
	public void hasNotValidId()
	{
		final FileDb file1 = new FileDb(-   1, "file.txt", "");
		assertFalse(file1.hasValidId());
		
		final FileDb file2 = new FileDb(-1000, "file.txt", "");
		assertFalse(file2.hasValidId());
	}
	
	@Test
	public void hasValidName()
	{
		final FileDb file1 = new FileDb(1, "file.txt", "");
		assertTrue(file1.hasValidName());
		
		final FileDb file2 = new FileDb(1, "a_file.pdf", "");
		assertTrue(file2.hasValidName());
		
		final FileDb file3 = new FileDb(1, "A_file.pdf", "");
		assertTrue(file3.hasValidName());
		
		final FileDb file4 = new FileDb(1, "A file.pdf", "");
		assertTrue(file4.hasValidName());
	}
	
	@Test
	public void hasNotValidName()
	{
		final FileDb file1 = new FileDb(1, "", "");
		assertFalse(file1.hasValidName());

		final FileDb file2 = new FileDb(1, null, "");
		assertFalse(file2.hasValidName());
	}
}
