/*
 * Copyright 2016  Nicola Spanti  <dev@nicola-spanti.info>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * https://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package br.unicamp.ic.lasca.unilea.utils;


import org.junit.Test;

import br.unicamp.ic.lasca.unilea.utils.UrlUtils;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertFalse;


public final class UrlUtilsTests
{
	@Test
	public void isHttp()
	{
		assertTrue(UrlUtils.isHttp("http://www.bortzmeyer.org/"));
		assertTrue(UrlUtils.isHttp("http://www.bortzmeyer.org/cryptage-n-existe-pas.html"));
	}
	
	@Test
	public void isNotHttp()
	{
		assertFalse(UrlUtils.isHttp("https://www.gnu.org/"));
		assertFalse(UrlUtils.isHttp("https://www.gnu.org/philosophy/proprietary.html"));
		
		assertFalse(UrlUtils.isHttp(""));
		assertFalse(UrlUtils.isHttp(null));
	}
	
	@Test
	public void isHttps()
	{
		assertTrue(UrlUtils.isHttps("https://www.gnu.org/"));
		assertTrue(UrlUtils.isHttps("https://www.gnu.org/philosophy/proprietary.html"));

		assertTrue(UrlUtils.isHttps("https://www.wikipedia.org/"));
		assertTrue(UrlUtils.isHttps("https://en.wikipedia.org/wiki/Java_%28programming_language%29"));
		assertTrue(UrlUtils.isHttps("https://en.wikipedia.org/wiki/Java_%28programming_language%29#Examples"));
	}
	
	@Test
	public void isNotHttps()
	{
		assertFalse(UrlUtils.isHttps("http://www.bortzmeyer.org/"));
		assertFalse(UrlUtils.isHttps("http://www.bortzmeyer.org/cryptage-n-existe-pas.html"));
		
		assertFalse(UrlUtils.isHttps(""));
		assertFalse(UrlUtils.isHttps(null));
	}
	
	@Test
	public void isWeb()
	{
		assertTrue(UrlUtils.isWeb("http://www.bortzmeyer.org/"));
		assertTrue(UrlUtils.isWeb("http://www.bortzmeyer.org/cryptage-n-existe-pas.html"));
		
		assertTrue(UrlUtils.isWeb("https://www.gnu.org/"));
		assertTrue(UrlUtils.isWeb("https://www.gnu.org/philosophy/proprietary.html"));

		assertTrue(UrlUtils.isWeb("https://www.wikipedia.org/"));
		assertTrue(UrlUtils.isWeb("https://en.wikipedia.org/wiki/Java_%28programming_language%29"));
		assertTrue(UrlUtils.isWeb("https://en.wikipedia.org/wiki/Java_%28programming_language%29#Examples"));
	}
	
	@Test
	public void isNotWeb()
	{
		assertFalse(UrlUtils.isWeb(""));
		assertFalse(UrlUtils.isWeb(null));
	}
}
