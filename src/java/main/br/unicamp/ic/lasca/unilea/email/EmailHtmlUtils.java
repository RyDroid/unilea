/*
 * Copyright 2016  Nicola Spanti  <dev@nicola-spanti.info>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * https://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package br.unicamp.ic.lasca.unilea.email;


import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;


public final class EmailHtmlUtils
{
	/**
	 * Don't let anyone instantiate this class.
	 */
	private
	EmailHtmlUtils()
	{}

	
	public static Set<String>
	getStringLinksFromPlainText(final String htmlPage)
	{
		if(htmlPage == null)
		{
			return null;
		}
		
		final Document doc = Jsoup.parse(htmlPage);
		final Elements linkElements = doc.select("a[href]");
		
		Set<String> links = new HashSet<String>();
		for (final Element linkElement : linkElements)
		{
			final String href = linkElement.attr("href");
			if(href != null && !href.isEmpty() && !href.startsWith("#"))
			{
				links.add(href);
			}
		}
		return links;
	}
	
	public static Collection< Set<String> >
	getStringLinksFromPlainText(final Collection<String> htmlPages)
	{
		final Collection< Set<String> > links = new ArrayList< Set<String> >();
		for(final String htmlPage : htmlPages)
		{
			links.add(getStringLinksFromPlainText(htmlPage));
		}
		return links;
	}
	
	public static Collection<String>
	getHtmlAsPlainTextFromEmailsContent(final Collection<EmailContent> emails)
	{
		if(emails == null)
		{
			return null;
		}
		
		final Collection<String> htmlPages = new ArrayList<String>();
		for(final EmailContent email : emails)
		{
			if(email == null)
			{
				htmlPages.add(null);
			}
			else
			{
				htmlPages.add(email.getHtmlAsPlainText());
			}
		}
		return htmlPages;
	}
}
