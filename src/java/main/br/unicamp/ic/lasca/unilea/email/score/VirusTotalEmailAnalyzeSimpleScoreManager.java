/*
 * Copyright 2016  Nicola Spanti  <dev@nicola-spanti.info>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * https://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package br.unicamp.ic.lasca.unilea.email.score;


import java.io.PrintStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import br.unicamp.ic.lasca.unilea.DefaultValues;
import br.unicamp.ic.lasca.unilea.utils.CommandLineArgumentsUtils;
import br.unicamp.ic.lasca.unilea.webpage.analyze.virustotal.VirusTotalWebPageAnalyzeSimpleScoreManager;


public class VirusTotalEmailAnalyzeSimpleScoreManager
extends EmailScoreManagerDbDefault
{
	public static final String ALGORITHM_NAME =
			"email_virus-total-public-v2_simple-add";
	
	
	private Connection connection = null;
	private final String insertScoreStringRequest =
			"INSERT INTO email_security_score_int(email_id, algorithm_id, value)\n"+
					"SELECT ? AS email_id,\n"+
					"       ? AS email_algorithm_id,\n"+
					"       COALESCE(SUM(value) / COUNT(value), 0) AS final_value\n"+
					"FROM   webpage_security_score_int\n"+
					"WHERE  webpage_id IN (\n"+
					"       SELECT webpage_id\n"+
					"       FROM   email_link\n"+
					"       WHERE  email_id = ?\n"+
					") AND  algorithm_id = (\n"+
					"       SELECT id\n"+
					"       FROM   security_score_algorithm\n"+
					"       WHERE  name = ?\n"+
					")\n"+
					"ON CONFLICT DO NOTHING";
	private PreparedStatement insertScoreStatement = null;
	
	
	public VirusTotalEmailAnalyzeSimpleScoreManager()
	{
		super(ALGORITHM_NAME);
	}
	
	
	public synchronized boolean
	init(final String url, final String user, final String password)
	{
		try
		{
			Class.forName("org.postgresql.Driver");
		}
		catch (final ClassNotFoundException e)
		{
			e.printStackTrace();
			return false;
		}
		
		if(connection == null)
		{
			try
			{
				connection = DriverManager.getConnection(
						url, user, password);
			}
			catch (final SQLException e)
			{
				e.printStackTrace();
				return false;
			}
		}

		if(insertScoreStatement == null)
		{
			try
			{
				insertScoreStatement =
						connection.prepareStatement(insertScoreStringRequest);
				insertScoreStatement.setString(4,
						VirusTotalWebPageAnalyzeSimpleScoreManager.ALGORITHM_NAME);
			}
			catch (final SQLException e)
			{
				e.printStackTrace();
				return false;
			}
		}
		
		return super.setDataBaseConnexion(connection) && super.init();
	}

	@Override
	public boolean
	init()
	{
		return init(
				DefaultValues.dataBaseUrl,
				DefaultValues.dataBaseUser,
				DefaultValues.dataBasePassword
				);
	}
	
	
	@Override
	public boolean
	insertScore(int emailId)
	{
		if(emailId < 0 || getAlgorithmId() < 0)
		{
			return false;
		}
		
		try
		{
			insertScoreStatement.setInt(1, emailId);
			insertScoreStatement.setInt(2, getAlgorithmId());
			insertScoreStatement.setInt(3, emailId);
			insertScoreStatement.setString(4,
					VirusTotalWebPageAnalyzeSimpleScoreManager.ALGORITHM_NAME);
			insertScoreStatement.executeUpdate();
		}
		catch (final SQLException e)
		{
			e.printStackTrace();
			return false;
		}
		
		notifyScoreInserted();
		return true;
	}
	
	
	private static void
	printHelp(final PrintStream stream)
	{
		stream.println("No argument needed");
	}
	
	private static void
	printHelp()
	{
		printHelp(System.out);
	}
	
	public static void
	main(String[] args)
	{
		if(args.length > 0 && CommandLineArgumentsUtils.isHelpArgument(args[0]))
		{
			printHelp();
			System.exit(0);
		}
		
		EmailScoreManagerDb scoreManager =
				new VirusTotalEmailAnalyzeSimpleScoreManager();
		scoreManager.init();
		scoreManager.destroy();
		
		int nb = scoreManager.computeScores();
		if(nb < 0)
		{
			System.err.println("Something wrong happened!");
			System.exit(1);
		}
		if(nb == 0)
		{
			System.out.println("No score to add");
		}
		else if(nb == 1)
		{
			System.out.println("1 score added");
		}
		else
		{
			System.out.println(nb +" scores added");
		}
	}
}
