/*
 * Copying and distribution of this file, with or without modification,
 * are permitted in any medium without royalty provided this notice is
 * preserved.  This file is offered as-is, without any warranty.
 * Names of contributors must not be used to endorse or promote products
 * derived from this file without specific prior written permission.
 */


package br.unicamp.ic.lasca.unilea.utils;


public final class UrlUtils
{
	/**
	 * Don't let anyone instantiate this class.
	 */
	private
	UrlUtils()
	{}
	
	
	public static boolean
	isHttp(final String url)
	{
		return url != null && url.toLowerCase().startsWith("http://");
	}
	
	public static boolean
	isHttps(final String url)
	{
		return url != null && url.toLowerCase().startsWith("https://");
	}
	
	public static boolean
	isWeb(final String url)
	{
		return isHttp(url) || isHttps(url);
	}
	
	public static boolean
	isLocal(final String url)
	{
		return url != null && (url.startsWith("/") || url.toLowerCase().startsWith("file://"));
	}
}
