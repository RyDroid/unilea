/*
 * Copying and distribution of this file, with or without modification,
 * are permitted in any medium without royalty provided this notice is
 * preserved.  This file is offered as-is, without any warranty.
 * Names of contributors must not be used to endorse or promote products
 * derived from this file without specific prior written permission.
 */


package br.unicamp.ic.lasca.unilea.utils;


import java.util.ArrayList;
import java.util.Collection;


public class ObservableDefault
implements Observable
{
	private Collection<Observer> observers = new ArrayList<Observer>();
	
	
	@Override
	public boolean
	addObserver(final Observer observer)
	{
		return observers.add(observer);
	}

	@Override
	public boolean
	removeObserver(final Observer observer)
	{
		return observers.remove(observer);
	}

	@Override
	public void
	removeObservers()
	{
		observers.clear();
	}
	
	@Override
	public int
	countObservers()
	{
		return observers.size();
	}
	
	@Override
	public int
	notifyObservers(final Object argument)
	{
		return ObservableUtils.notifyObserversFromIterable(
				this, observers, argument
				);
	}
}
