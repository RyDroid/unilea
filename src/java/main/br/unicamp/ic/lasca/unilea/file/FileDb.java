/*
 * Copyright 2016  Nicola Spanti  <dev@nicola-spanti.info>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * https://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package br.unicamp.ic.lasca.unilea.file;


import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.ResultSet;
import java.sql.SQLException;


public class FileDb
{
	private int    _id   = -1;
	private String _name = null;
	private File   _file = null;
	
	
	public FileDb(int id, final String name, final String payload)
	{
		_id   =  id;
		_name = name;
		
		if(createFileForce())
		{
			PrintWriter writer = null;
			try
			{
				writer = new PrintWriter(_file);
			}
			catch (final FileNotFoundException e)
			{
				e.printStackTrace();
			}
			
			if(writer == null || payload == null || payload.trim().isEmpty())
			{
				_file.delete();
				_file = null;
			}
			else
			{
				writer.print(payload);
				writer.flush();
			}
		}
	}
	
	public FileDb(final ResultSet res) throws SQLException
	{
		this(res.getInt("id"), res.getString("name"), res.getString("payload"));
	}
	
	
	public boolean
	hasValidId()
	{
		return _id >= 0;
	}
	
	public int
	getId()
	{
		return _id;
	}
	
	public boolean
	hasValidName()
	{
		return _name != null && !_name.isEmpty();
	}
	
	public String
	getName()
	{
		return _name;
	}
	
	public File
	getFile()
	{
		return _file;
	}
	
	
	private boolean
	createFileForce()
	{
		if(_file != null)
		{
			return true;
		}
		
		if(hasValidId() && hasValidName())
		{
			try
			{
				_file = File.createTempFile("unilea_tmp_", "_"+ _name);
			}
			catch (final IOException e)
			{
				e.printStackTrace();
				return false;
			}
			
			_file.deleteOnExit();
			return _file.canWrite();
		}
		return false;
	}
	
	@SuppressWarnings("unused")
	private boolean
	createFile()
	{
		return _file == null ? createFileForce() : true;
	}
}
