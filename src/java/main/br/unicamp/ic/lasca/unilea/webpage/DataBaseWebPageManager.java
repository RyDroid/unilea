/*
 * Copyright 2016  Nicola Spanti  <dev@nicola-spanti.info>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * https://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package br.unicamp.ic.lasca.unilea.webpage;


import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import br.unicamp.ic.lasca.unilea.utils.UrlUtils;


public class DataBaseWebPageManager
{
	public static final String insertStringRequest =
			"INSERT INTO webpage (url) VALUES (?) ON CONFLICT DO NOTHING";
	public static final String selectIdWithUrlStringRequest =
			"(SELECT id FROM webpage WHERE url = ?)";
	
	
	private Connection _connection = null;
	private PreparedStatement insertStatement = null;
	private PreparedStatement selectIdWithUrlStatement = null;
	
	
	public
	DataBaseWebPageManager(final Connection connection)
	{
		_connection = connection;
	}
	
	
	public boolean
	insert(final String url)
	{
		if(!UrlUtils.isWeb(url))
		{
			return false;
		}
		
		if(insertStatement == null)
		{
			try
			{
				insertStatement =
						_connection.prepareStatement(insertStringRequest);
			}
			catch (final SQLException e)
			{
				e.printStackTrace();
				return false;
			}
		}
		
		try
		{
			insertStatement.setString(1, url);
			insertStatement.executeUpdate();
			return true;
		}
		catch (final SQLException e)
		{
			e.printStackTrace();
			return false;
		}
	}
	
	public int
	getId(final String url)
	{
		if(url == null || url.isEmpty())
		{
			return -1;
		}

		if(selectIdWithUrlStatement == null)
		{
			try
			{
				selectIdWithUrlStatement =
						_connection.prepareStatement(selectIdWithUrlStringRequest);
			}
			catch (final SQLException e)
			{
				e.printStackTrace();
				return -1;
			}
		}
		
		try 
		{
			selectIdWithUrlStatement.setString(1, url);
			final ResultSet result = selectIdWithUrlStatement.executeQuery();
			if(result.next())
			{
				int res = result.getInt("id");
				result.close();
				return res;
			}
			else
			{
				result.close();
				return -1;
			}
		}
		catch (final SQLException e)
		{
			e.printStackTrace();
			return -1;
		}
	}
	
	public int
	insertAndGetId(final String url)
	{
		if(!insert(url))
		{
			return -1;
		}
		return getId(url);
	}
}
