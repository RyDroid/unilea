/*
 * Copyright 2016  Nicola Spanti  <dev@nicola-spanti.info>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * https://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package br.unicamp.ic.lasca.unilea.email;


import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.Set;


public final class EmailLinksCsvUtils
{

	/**
	 * Don't let anyone instantiate this class.
	 */
	private
	EmailLinksCsvUtils()
	{}
	
	
	public static boolean
	putHeader(final PrintWriter writer)
	{
		if(writer == null)
		{
			return false;
		}
		writer.println("idemail;link");
		return true;
	}
	
	public static PrintWriter
	getWriterWithHeader(final String fileName)
	{
		PrintWriter csvWriter;
		try
		{
			csvWriter = new PrintWriter(fileName);
		}
		catch (final FileNotFoundException e)
		{
			e.printStackTrace();
			return null;
		}
		
		if(!putHeader(csvWriter))
		{
			return null;
		}
		return csvWriter;
	}
	
	
	public static boolean
	putLink(final PrintWriter writer, int idEmail, final String urlString)
	{
		if(writer == null || idEmail < 0 || urlString == null || urlString.isEmpty())
		{
			return false;
		}
		
		writer.println(idEmail +";"+ urlString);
		return true;
	}
	
	public static int
	putLinks(final PrintWriter writer, int idEmail, final Set<String> links)
	{
		if(links == null)
		{
			return -1;
		}
		
		int nb = 0;
		for(final String urlString : links)
		{
			if(putLink(writer, idEmail, urlString))
			{
				++nb;
			}
		}
		return nb;
	}
}
