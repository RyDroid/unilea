/*
 * Copyright 2016  Nicola Spanti  <dev@nicola-spanti.info>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * https://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package br.unicamp.ic.lasca.unilea.utils;


public final class CommandLineArgumentsUtils
{
	/**
	 * Don't let anyone instantiate this class.
	 */
	private
	CommandLineArgumentsUtils()
	{}
	
	
	public static boolean
	isHelpArgument(String arg)
	{
		if(arg == null)
		{
			return false;
		}
		arg = arg.toLowerCase();
		return "-?".equals(arg) || "-h".equals(arg) || "--help".equals(arg) || "help".equals(arg);
	}
}
