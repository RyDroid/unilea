/*
 * Copyright 2016  Nicola Spanti  <dev@nicola-spanti.info>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * https://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package br.unicamp.ic.lasca.unilea.file.analyze.virustotal;


import java.io.PrintStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Collection;
import java.util.Map;

import br.unicamp.ic.lasca.unilea.DefaultValues;
import br.unicamp.ic.lasca.unilea.file.FileDb;
import br.unicamp.ic.lasca.unilea.file.analyze.DataBaseFileAnalyzeKeyValueManager;
import br.unicamp.ic.lasca.unilea.genericanalyze.AnalyzeKeyValueInterface;
import br.unicamp.ic.lasca.unilea.genericanalyze.DataBaseGenericAnalyzeKeyValueManager;
import br.unicamp.ic.lasca.unilea.genericanalyze.WaiterObserverToNotifyAnalyzer;
import br.unicamp.ic.lasca.unilea.genericanalyze.virustotal.VirusTotalGenericAnalyze;
import br.unicamp.ic.lasca.unilea.utils.CommandLineArgumentsUtils;
import br.unicamp.ic.lasca.unilea.utils.CounterWaiter;
import br.unicamp.ic.lasca.unilea.utils.Observable;
import br.unicamp.ic.lasca.unilea.utils.Observer;
import br.unicamp.ic.lasca.unilea.utils.WaiterObserverToCommitBefore;
import br.unicamp.ic.lasca.unilea.virustotal.VirusTotalPublicV2Implementation;
import br.unicamp.ic.lasca.unilea.virustotal.VirusTotalPublicV2Interface;
import br.unicamp.ic.lasca.unilea.virustotal.VirusTotalV2Waiter;

import com.kanishka.virustotal.dto.FileScanReport;
import com.kanishka.virustotal.dto.ScanInfo;
import com.kanishka.virustotal.dto.VirusScanInfo;
import com.kanishka.virustotal.exception.APIKeyNotFoundException;
import com.kanishka.virustotal.exception.QuotaExceededException;


public class VirusTotalFileAnalyze
extends VirusTotalGenericAnalyze<FileDb>
{
	public static final String ANALYZER_NAME = "file_virus-total-public-v2";
	
	private static CounterWaiter requestWaiter = VirusTotalV2Waiter.get();
	private static VirusTotalPublicV2Interface virusTotalRef = null;
	private static DataBaseGenericAnalyzeKeyValueManager<FileDb> analyzeKeyValueManager = null;
	
	
	/**
	 * Don't let anyone instantiate this class.
	 */
	private
	VirusTotalFileAnalyze()
	{
		requestWaiter.addObserver(new WaiterObserverToNotifyAnalyzer(this));
		addObserver(new WaiterObserverToCommitBefore(this));
	}
	
	private
	VirusTotalFileAnalyze(boolean verbose)
	{
		this();
		
		if(verbose)
		{
			addObserver(new Observer()
			{
				@Override
				public boolean update(final Observable observable, final Object argument)
				{
					if(argument instanceof FileDb)
					{
						final FileDb file = (FileDb) argument;
						System.out.println(file.getName() +" - DONE");
						System.out.flush();
						return true;
					}
					return false;
				}
			});
		}
	}
	
	
	@SuppressWarnings("unused")
	private synchronized boolean
	init(
			final String apiKey,
			final String url,
			final String user,
			final String password)
	{
		setVirusTotalAPIKey(apiKey);
		return init(url, user, password);
	}
	
	private boolean
	init(final String url, final String user, final String password)
	{
		try
		{
			Class.forName("org.postgresql.Driver");
		}
		catch (final ClassNotFoundException e)
		{
			e.printStackTrace();
			return false;
		}
		
		Connection connexion;
		try
		{
			connexion = DriverManager.getConnection(
					url, user, password
					);
			connexion.setAutoCommit(false);
		}
		catch (final SQLException e)
		{
			e.printStackTrace();
			return false;
		}
		analyzeKeyValueManager =
				new DataBaseFileAnalyzeKeyValueManager(
						connexion, ANALYZER_NAME
						);
		if(!analyzeKeyValueManager.init())
		{
			return false;
		}
		
		try
		{
			virusTotalRef = new VirusTotalPublicV2Implementation();
		}
		catch (final APIKeyNotFoundException e)
		{
			e.printStackTrace();
			return false;
		}
		
		return
				analyzeKeyValueManager.insertAnalyzerInDb() &&
				analyzeKeyValueManager.getAnalyzerIdOfDb() >= 0;
	}
	
	private boolean
	init(final String apiKey)
	{
		setVirusTotalAPIKey(apiKey);
		return init();
	}
	
	private boolean
	init()
	{
		return init(
				DefaultValues.dataBaseUrl,
				DefaultValues.dataBaseUser,
				DefaultValues.dataBasePassword
				);
	}
	
	@Override
	public boolean
	commit()
	{
		return analyzeKeyValueManager.commit();
	}
	
	private synchronized boolean
	destroy()
	{
		if(analyzeKeyValueManager == null)
		{
			return true;
		}
		
		boolean success = analyzeKeyValueManager.destroy();
		analyzeKeyValueManager = null;
		return success;
	}
	
	
	@Override
	protected int
	insertAnalyzeScans(
			final FileDb file,
			final Map<String, VirusScanInfo> scans)
	{
		if(analyzeKeyValueManager == null)
		{
			return -1;
		}
		
		final Collection<AnalyzeKeyValueInterface> keyValues =
				scansToCollectionAnalyzeKeyValue(scans);
		replaceNullValuesOfCollectionAnalyzeKeyValue(
				keyValues, "unrated");
		return analyzeKeyValueManager.insertCollectionAnalyzeKeyValue(
				file, keyValues);
	}
	
	private int
	insertAnalyzeReport(
			final FileDb file,
			final FileScanReport report)
	{
		return insertAnalyzeScans(file, report);
	}
	
	
	private int
	scanElementAndPutReportInDb(
			final FileDb file,
			boolean isRetry)
	{
		if(!requestWaiter.waitIfNeeded())
		{
			return -1;
		}
		
		FileScanReport report;
		try
		{
			final ScanInfo info = virusTotalRef.scanFile(file.getFile());
			report = virusTotalRef.getScanReport(info.getResource());
		}
		catch (final QuotaExceededException e)
		{
			requestWaiter.waitMaximum();
			return isRetry ? -1 : scanElementAndPutReportInDb(file);
		}
		catch (final Exception e)
		{
			e.printStackTrace();
			return -1;
		}
		requestWaiter.increment();
		return insertAnalyzeReport(file, report);
	}

	@Override
	protected int
	scanElementAndPutReportInDb(final FileDb file)
	{
		return scanElementAndPutReportInDb(file, false);
	}
	
	@Override
	protected int
	analyzeSomeUnalizedElementsAndPutReportsInDb(int limit)
	{
		return scanElementsAndPutReportInDb(
				analyzeKeyValueManager.getSomeUnalizedElements(limit)
				);
	}
	
	
	public static void
	printHelp(final PrintStream stream)
	{
		if(stream != null)
		{
			stream.println("1 argument needed: the API key");
		}
	}
	
	public static void
	printHelp()
	{
		printHelp(System.out);
	}
	
	public static void
	main(final String[] args)
	{
		if(args.length == 0)
		{
			printHelp(System.err);
			System.exit(1);
		}
		
		boolean verbose = false;
		boolean onlyPrintNbUnalized = false;
		for(int i=0; i < args.length -1; ++i)
		{
			if(CommandLineArgumentsUtils.isHelpArgument(args[0]))
			{
				printHelp();
				System.exit(0);
			}
			
			if(args[i].equals("-v") || args[i].equals("--verbose"))
			{
				verbose = true;
			}
			else if(args[i].equals("--quiet"))
			{
				verbose = false;
			}
			else if(args[i].equals("--analize"))
			{
				onlyPrintNbUnalized = false;
			}
			else if(args[i].equals("--nb-unanalized"))
			{
				onlyPrintNbUnalized = true;
			}
			else
			{
				System.err.println("Argument "+ args[i] +" not managed.");
			}
		}
		if(CommandLineArgumentsUtils.isHelpArgument(args[args.length -1]))
		{
			printHelp();
			System.exit(0);
		}
		
		final String apiKey = args[args.length -1];
		if(!isValidAPIKey(apiKey))
		{
			System.err.println("The API key is not valid");
			System.exit(1);
		}
		
		final VirusTotalFileAnalyze analyze = new VirusTotalFileAnalyze(verbose);
		if(!analyze.init(apiKey))
		{
			System.err.println("Initialization failed! :(");
			System.exit(1);
		}
		
		int nb = -1;
		if(onlyPrintNbUnalized)
		{
			// TODO
			System.err.println("TODO");
			System.exit(0);
		}
		else
		{
			nb = analyze.analyzeSomeUnalizedElementsAndPutReportsInDb(5);
		}
		analyze.destroy();
		
		if(nb < 0)
		{
			System.err.println("An error occured.");
		}
		else if(onlyPrintNbUnalized && nb < 2)
		{
			System.out.println(nb +" file unanalyzed.");
		}
		else if(onlyPrintNbUnalized && nb > 1)
		{
			System.out.println(nb +" files unanalyzed.");
		}
		else if(nb < 2)
		{
			System.out.println(nb +" file analyzed successfully.");
		}
		else
		{
			System.out.println(nb +" files analyzed successfully.");
		}
	}
}
