/*
 * Copyright 2016  Nicola Spanti  <dev@nicola-spanti.info>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * https://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package br.unicamp.ic.lasca.unilea.webpage.analyze;


import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;

import br.unicamp.ic.lasca.unilea.genericanalyze.AnalyzeKeyValueInterface;
import br.unicamp.ic.lasca.unilea.genericanalyze.DataBaseGenericAnalyzeKeyValueManager;
import br.unicamp.ic.lasca.unilea.webpage.WebPageDb;


public class DataBaseWebPageAnalyzeKeyValueManager
extends DataBaseGenericAnalyzeKeyValueManager<WebPageDb>
{
	private Connection connection = null;
	private static final String getUnanlyzedWebPagesStringRequest =
			"SELECT id, url\n"+
					"FROM webpage\n"+
					"WHERE id NOT IN("+
					"      SELECT DISTINCT webpage_id\n"+
					"      FROM webpage_analyze_key_value\n"+
					"      INNER JOIN security_analyzer\n"+
					"                 ON security_analyzer.id = analyzer_id\n"+
					"      WHERE security_analyzer.name = ?\n"+
					")\n"+
					"LIMIT ?";
	private static final String insertWebPageAnalyzeKeyValueStringRequest =
			"INSERT INTO webpage_analyze_key_value (webpage_id, analyzer_id, key, value)\n"+
					"VALUES(?, ?, ?, ?)\n"+
					"ON CONFLICT DO NOTHING";
	private PreparedStatement getUnanlyzedWebPagesStatement          = null;
	private PreparedStatement insertWebPageAnalyzeKeyValueStatement  = null;
	
	
	public
	DataBaseWebPageAnalyzeKeyValueManager(
			final Connection aConnection,
			final String theAnalyzerName)
	{
		super(aConnection, theAnalyzerName);
		connection = aConnection;
	}
	
	
	public boolean
	init()
	{
		if(!super.init())
		{
			return false;
		}

		if(getUnanlyzedWebPagesStatement == null)
		{
			try
			{
				getUnanlyzedWebPagesStatement =
						connection.prepareStatement(
								getUnanlyzedWebPagesStringRequest);
			}
			catch (final SQLException e)
			{
				e.printStackTrace();
				return false;
			}
		}

		if(insertWebPageAnalyzeKeyValueStatement == null)
		{
			try
			{
				insertWebPageAnalyzeKeyValueStatement =
						connection.prepareStatement(
								insertWebPageAnalyzeKeyValueStringRequest);
			}
			catch (final SQLException e)
			{
				e.printStackTrace();
				return false;
			}
		}

		return true;
	}
	
	@Override
	public boolean
	commit()
	{
		if(connection == null)
		{
			return false;
		}
		
		try
		{
			if(!connection.getAutoCommit())
			{
				connection.commit();
			}
			return true;
		}
		catch (final Exception e)
		{
			e.printStackTrace();
			return false;
		}
	}
	
	public boolean
	destroy()
	{
		if(connection != null)
		{
			commit();
			try
			{
				connection.close();
			}
			catch (final SQLException e)
			{
				e.printStackTrace();
				return false;
			}
		}
		return true;
	}
	
	
	public static Collection<WebPageDb>
	getUnalizedWebPages(final ResultSet results)
	{
		final Collection<WebPageDb> webpages = new ArrayList<WebPageDb>();
		try
		{
			while(results.next())
			{
				webpages.add(new WebPageDb(results));
			}
			results.close();
		}
		catch (final SQLException e)
		{
			e.printStackTrace();
			return null;
		}
		return webpages;
	}
	
	@Override
	public Collection<WebPageDb>
	getSomeUnalizedElements(int limit)
	{
		ResultSet results = null;
		try
		{
			getUnanlyzedWebPagesStatement.setString(1, getAnalyzerName());
			if(limit > 0)
			{
				getUnanlyzedWebPagesStatement.setInt(2, limit);
			}
			else
			{
				getUnanlyzedWebPagesStatement.setInt(2, Integer.MAX_VALUE);
			}
			results = getUnanlyzedWebPagesStatement.executeQuery();
		}
		catch (final SQLException e)
		{
			e.printStackTrace();
			return null;
		}
		return getUnalizedWebPages(results);
	}
	
	
	public boolean
	insertAnalyzeKeyValue(
			int webpage_id,
			final String key,
			final String value)
	{
		try
		{
			insertWebPageAnalyzeKeyValueStatement.setInt   (1, webpage_id);
			insertWebPageAnalyzeKeyValueStatement.setInt   (2, getAnalyzerIdOfDb());
			insertWebPageAnalyzeKeyValueStatement.setString(3, key);
			insertWebPageAnalyzeKeyValueStatement.setString(4, value);
			insertWebPageAnalyzeKeyValueStatement.executeUpdate();
		}
		catch (final SQLException e)
		{
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
	public boolean
	insertAnalyzeKeyValue(
			final WebPageDb webpage,
			final String key,
			final String value)
	{
		return insertAnalyzeKeyValue(webpage.getId(), key, value);
	}
	
	@Override
	public boolean
	insertAnalyzeKeyValue(
			final WebPageDb webpage,
			final AnalyzeKeyValueInterface keyValue)
	{
		return insertAnalyzeKeyValue(webpage, keyValue.getKey(), keyValue.getValue());
	}
}
