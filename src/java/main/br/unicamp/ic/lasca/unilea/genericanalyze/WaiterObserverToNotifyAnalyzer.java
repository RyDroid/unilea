/*
 * Copyright 2016  Nicola Spanti  <dev@nicola-spanti.info>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * https://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package br.unicamp.ic.lasca.unilea.genericanalyze;


import br.unicamp.ic.lasca.unilea.utils.Observable;
import br.unicamp.ic.lasca.unilea.utils.Observer;
import br.unicamp.ic.lasca.unilea.utils.WaiterObserverEventType;


public class WaiterObserverToNotifyAnalyzer
implements Observer
{
	private Observable _observable;
	
	public WaiterObserverToNotifyAnalyzer(
			final Observable observable)
	{
		_observable = observable;
	}
	
	@Override
	public boolean update(final Observable observable, final Object argument)
	{
		if(argument == WaiterObserverEventType.BEFORE)
		{
			_observable.notifyObservers(AnalyzeObserverEventType.BEFORE_WAIT);
			return true;
		}
		if(argument == WaiterObserverEventType.AFTER)
		{
			_observable.notifyObservers(AnalyzeObserverEventType.AFTER_WAIT);
			return true;
		}
		return false;
	}
}
