/*
 * Copyright 2016  Nicola Spanti  <dev@nicola-spanti.info>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * https://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package br.unicamp.ic.lasca.unilea.email;


import java.io.PrintStream;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import br.unicamp.ic.lasca.unilea.DefaultValues;
import br.unicamp.ic.lasca.unilea.utils.CommandLineArgumentsUtils;


public final class EmailLinksDbToCsv
{
	/**
	 * Don't let anyone instantiate this class.
	 */
	private
	EmailLinksDbToCsv()
	{}
	

	private static Connection connection = null;
	private static final String getEmailLinksStringRequest =
			"SELECT email_id, url\n"+
					"FROM email_link\n"+
					"INNER JOIN webpage ON (webpage.id = webpage_id)\n"+
					"ORDER BY email_id, url";
	private static PreparedStatement getEmailLinksStatement = null;
	
	
	private static synchronized boolean
	init(final String url, final String user, final String password)
	{
		try
		{
			Class.forName("org.postgresql.Driver");
		}
		catch (final ClassNotFoundException e)
		{
			e.printStackTrace();
			return false;
		}
		
		try
		{
			connection = DriverManager.getConnection(
					url, user, password);
		}
		catch (final SQLException e)
		{
			e.printStackTrace();
			return false;
		}
		
		try
		{
			getEmailLinksStatement =
					connection.prepareStatement(getEmailLinksStringRequest);
		}
		catch (final SQLException e)
		{
			e.printStackTrace();
			return false;
		}
		
		return true;
	}
	
	private static boolean
	init()
	{
		return init(
				DefaultValues.dataBaseUrl,
				DefaultValues.dataBaseUser,
				DefaultValues.dataBasePassword
				);
	}
	
	private static synchronized boolean
	destroy()
	{
		if(connection != null)
		{
			try
			{
				connection.close();
			}
			catch (final SQLException e)
			{
				e.printStackTrace();
				return false;
			}
		}
		return true;
	}
	
	
	public static ResultSet
	getLinks()
	{
		try
		{
			return getEmailLinksStatement.executeQuery();
		}
		catch (final SQLException e)
		{
			e.printStackTrace();
			return null;
		}
	}
	
	public static boolean
	putLinkInFile(final PrintWriter csvWriter, final ResultSet result)
	{
		try
		{
			return EmailLinksCsvUtils.putLink(
					csvWriter,
					result.getInt   ("email_id"),
					result.getString("url")
					);
		}
		catch (final SQLException e)
		{
			e.printStackTrace();
			return false;
		}
	}
	
	public static int
	putLinksInFile(final PrintWriter csvWriter, final ResultSet results)
	{
		if(results == null)
		{
			return -1;
		}
		
		int nb = 0;
		try
		{
			while(results.next())
			{
				if(putLinkInFile(csvWriter, results))
				{
					++nb;
				}
			}
			results.close();
		}
		catch (final SQLException e)
		{
			e.printStackTrace();
		}
		return nb;
	}
	
	public static int
	putLinksInFile(final String fileName)
	{
		final PrintWriter csvWriter =
				EmailLinksCsvUtils.getWriterWithHeader(fileName);
		if(csvWriter == null)
		{
			return -1;
		}
		
		return putLinksInFile(csvWriter, getLinks());
	}
	
	
	public static void
	printHelp(final PrintStream stream)
	{
		if(stream != null)
		{
			stream.println("Only 1 argument needed: a CSV file path");
		}
	}
	
	public static void
	printHelp()
	{
		printHelp(System.out);
	}
	
	public static void
	main(final String[] args)
	{
		if(args.length != 1)
		{
			printHelp(System.err);
			System.exit(1);
		}
		if(CommandLineArgumentsUtils.isHelpArgument(args[0]))
		{
			printHelp();
			System.exit(0);
		}
		if(!init())
		{
			System.err.println("Initialization failed!");
			System.exit(1);
		}
		
		int nb = putLinksInFile(args[0]);
		destroy();
		
		if(nb < 0)
		{
			System.err.println("Something wrong happened!");
			System.exit(1);
		}
		if(nb == 0)
		{
			System.out.println("No record putted");
		}
		else if(nb == 1)
		{
			System.out.println("1 record putted");
		}
		else
		{
			System.out.println(nb +" records putted");
		}
	}
}
