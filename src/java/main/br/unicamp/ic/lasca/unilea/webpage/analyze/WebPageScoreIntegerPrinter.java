/*
 * Copyright 2016  Nicola Spanti  <dev@nicola-spanti.info>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * https://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package br.unicamp.ic.lasca.unilea.webpage.analyze;


import java.io.PrintStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import br.unicamp.ic.lasca.unilea.DefaultValues;
import br.unicamp.ic.lasca.unilea.utils.CommandLineArgumentsUtils;


public class WebPageScoreIntegerPrinter
{
	/**
	 * Don't let anyone instantiate this class.
	 */
	private
	WebPageScoreIntegerPrinter()
	{}
	
	
	private static  Connection connection = null;
	private static final String getSomeScoresStringRequest =
			"SELECT url, name, value\n"+
					"FROM webpage_security_score_int\n"+
					"INNER JOIN webpage\n"+
					"           ON webpage_id = webpage.id\n"+
					"INNER JOIN security_score_algorithm\n"+
					"           ON algorithm_id = security_score_algorithm.id\n"+
					"ORDER BY url, name\n"+
					"LIMIT ? OFFSET ?";
	private static PreparedStatement getSomeScoresStatement = null;
	private static boolean verbose = false;
	
	
	private static synchronized boolean
	init(final String url, final String user, final String password)
	{
		try
		{
			Class.forName("org.postgresql.Driver");
		}
		catch (final ClassNotFoundException e)
		{
			e.printStackTrace();
			return false;
		}
		
		if(connection == null)
		{
			try
			{
				connection = DriverManager.getConnection(
						url, user, password);
			}
			catch (final SQLException e)
			{
				e.printStackTrace();
				return false;
			}
		}
		
		if(getSomeScoresStatement == null)
		{
			try
			{
				getSomeScoresStatement =
						connection.prepareStatement(getSomeScoresStringRequest);
			}
			catch (final SQLException e)
			{
				e.printStackTrace();
				return false;
			}
		}
		
		return true;
	}
	
	private static boolean
	init()
	{
		return init(
				DefaultValues.dataBaseUrl,
				DefaultValues.dataBaseUser,
				DefaultValues.dataBasePassword
				);
	}
	
	private static synchronized boolean
	destroy()
	{
		if(connection != null)
		{
			try
			{
				connection.close();
			}
			catch (final SQLException e)
			{
				e.printStackTrace();
				return false;
			}
			connection = null;
		}
		return true;
	}
	
	
	private static ResultSet
	getSome(int limit, int offset)
	{
		if(getSomeScoresStatement == null)
		{
			return null;
		}
		
		try
		{
			if(limit >= 0)
			{
				getSomeScoresStatement.setInt(1, limit);
			}
			else
			{
				getSomeScoresStatement.setInt(1, Integer.MAX_VALUE);
			}
			
			if(offset >= 0)
			{
				getSomeScoresStatement.setInt(2, offset);
			}
			else
			{
				getSomeScoresStatement.setInt(2, Integer.MAX_VALUE);
			}
			
			return getSomeScoresStatement.executeQuery();
		}
		catch (SQLException e)
		{
			if(verbose)
			{
				e.printStackTrace();
			}
			return null;
		}
	}
	
	private static boolean
	printSome(final ResultSet results, final PrintStream stream)
	{
		if(results == null || stream == null)
		{
			return false;
		}
		
		try
		{
			while(results.next())
			{
				stream.println("* url:       "+ results.getString("url"));
				stream.println("  algorithm: "+ results.getString("name"));
				stream.println("  score:     "+ results.getString("value"));
			}
		}
		catch (SQLException e)
		{
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
	private static boolean
	printSome(final ResultSet results)
	{
		return printSome(results, System.out);
	}
	
	private static boolean
	printSome(int limit, int offset, final PrintStream stream)
	{
		return printSome(getSome(limit, offset), stream);
	}
	
	private static boolean
	printSome(int limit, int offset)
	{
		return printSome(getSome(limit, offset));
	}
	
	@SuppressWarnings("unused")
	private static boolean
	printAll(final PrintStream stream)
	{
		return printSome(Integer.MAX_VALUE, 0, stream);
	}
	
	private static boolean
	printAll()
	{
		return printSome(Integer.MAX_VALUE, 0);
	}
	
	
	private static void
	printHelp(final PrintStream stream)
	{
		if(stream != null)
		{
			stream.println("Just execute!");
		}
	}
	
	private static void
	printHelp()
	{
		printHelp(System.out);
	}
	
	public static void
	main(final String[] args)
	{
		// TODO format CSV
		for(int i=0; i < args.length; ++i)
		{
			if(CommandLineArgumentsUtils.isHelpArgument(args[i]))
			{
				printHelp();
				System.exit(0);
			}
			
			if(args[i].equals("-v") || args[i].equals("--verbose"))
			{
				verbose = true;
			}
			else if(args[i].equals("--quiet"))
			{
				verbose = false;
			}
			else
			{
				System.err.println("Argument "+ args[i] +" not managed.");
			}
		}
		
		if(!init())
		{
			System.err.println("Initialization failed! :(");
			System.exit(1);
		}
		
		if(!printAll())
		{
			System.err.println("There was at least one problem for printing.");
		}
		
		destroy();
	}
}
