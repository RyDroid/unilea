/*
 * Copyright 2016  Nicola Spanti  <dev@nicola-spanti.info>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * https://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package br.unicamp.ic.lasca.unilea.email;


import java.io.Serializable;


@SuppressWarnings("serial")
public final class EmailContent
implements Serializable
{
	private String _text = null;
	private String _html = null;
	private int    _id   = -1;
	
	
	public EmailContent(final String text, final String html)
	{
		_text = text;
		_html = html;
	}
	
	public EmailContent(final String text, final String html, int id)
	{
		this(text, html);
		_id = id;
	}
	
	
	public String
	getPlainText()
	{
		return _text;
	}
	
	public String
	getHtmlAsPlainText()
	{
		return _html;
	}
	
	public int
	getId()
	{
		return _id;
	}
}
