/*
 * Copying and distribution of this file, with or without modification,
 * are permitted in any medium without royalty provided this notice is
 * preserved.  This file is offered as-is, without any warranty.
 * Names of contributors must not be used to endorse or promote products
 * derived from this file without specific prior written permission.
 */


package br.unicamp.ic.lasca.unilea.utils;


import java.util.Iterator;


public final class ObservableUtils
{
	/**
	 * Don't let anyone instantiate this class.
	 */
	private ObservableUtils()
	{}
	
	
	public static
	int
	notifyObserversFromIterator(
			final Observable anObservable,
			final Iterator<Observer> observersIterator,
			final Object argument
			)
	{
		int nb = 0;
		while(observersIterator.hasNext())
		{
			final Observer observer = observersIterator.next();
			if(observer.update(anObservable, argument))
			{
				++nb;
			}
		}
		return nb;
	}
	
	public static
	int
	notifyObserversFromIterable(
			final Observable anObservable,
			final Iterable<Observer> someObservers,
			final Object argument
			)
	{
		return notifyObserversFromIterator(
				anObservable, someObservers.iterator(), argument
				);
	}
}
