/*
 * Copyright 2016  Nicola Spanti  <dev@nicola-spanti.info>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * https://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package br.unicamp.ic.lasca.unilea.virustotal;


import br.unicamp.ic.lasca.unilea.utils.CounterWaiter;

import com.kanishka.virustotalv2.VirustotalPublicV2;


public class VirusTotalV2Waiter
extends CounterWaiter
{
	public static final int NB_REQUESTS_PER_MINUTE =
			VirustotalPublicV2.VT2_MAX_ALLOWED_URLS_PER_REQUEST;
	public static final int TIME_TO_WAIT_MS = 60 * 1000;
	
	private static VirusTotalV2Waiter instance =  null;
	
	
	private VirusTotalV2Waiter()
	{
		super(NB_REQUESTS_PER_MINUTE, TIME_TO_WAIT_MS);
	}
	
	
	public static synchronized VirusTotalV2Waiter
	get()
	{
		if(instance == null)
		{
			instance = new VirusTotalV2Waiter();
		}
		return instance;
	}
}
