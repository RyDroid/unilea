/*
 * Copyright 2016  Nicola Spanti  <dev@nicola-spanti.info>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * https://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package br.unicamp.ic.lasca.unilea.genericanalyze.virustotal;


import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;

import br.unicamp.ic.lasca.unilea.file.analyze.virustotal.FileAnalyzeKeyValueVirusTotal;
import br.unicamp.ic.lasca.unilea.genericanalyze.AnalyzeKeyValueInterface;
import br.unicamp.ic.lasca.unilea.utils.Commitable;
import br.unicamp.ic.lasca.unilea.utils.ObservableDefault;

import com.kanishka.virustotal.dto.FileScanReport;
import com.kanishka.virustotal.dto.VirusScanInfo;
import com.kanishka.virustotalv2.VirusTotalConfig;


public abstract class VirusTotalGenericAnalyze<E>
extends ObservableDefault
implements Commitable
{
	protected static boolean
	isValidAPIKey(final String apiKey)
	{
		return apiKey != null && !apiKey.isEmpty();
	}
	
	protected static void
	setVirusTotalAPIKey(final String apiKey)
	{
		VirusTotalConfig.getConfigInstance().setVirusTotalAPIKey(apiKey);
	}
	
	
	protected static Collection<AnalyzeKeyValueInterface>
	scansToCollectionAnalyzeKeyValue(final Map<String, VirusScanInfo> scans)
	{
		if(scans == null)
		{
			return null;
		}
		
		final Collection<AnalyzeKeyValueInterface> results =
				new ArrayList<AnalyzeKeyValueInterface>();
		for(final Map.Entry<String, VirusScanInfo> scan : scans.entrySet())
		{
			results.add(new FileAnalyzeKeyValueVirusTotal(scan));
		}
		return results;
	}
	
	protected static int
	replaceNullValuesOfCollectionAnalyzeKeyValue(
			final Collection<AnalyzeKeyValueInterface> keyValues,
			final String defaultValue)
	{
		if(keyValues == null)
		{
			return -1;
		}
		
		int nb = 0;
		for(final AnalyzeKeyValueInterface keyValue : keyValues)
		{
			if(keyValue != null && keyValue.getValue() == null)
			{
				if(keyValue.setValue(defaultValue))
				{
					++nb;
				}
			}
		}
		return nb;
	}
	
	
	protected abstract int
	insertAnalyzeScans(
			final E element,
			final Map<String, VirusScanInfo> scans);
	
	protected int
	insertAnalyzeScans(
			final E element,
			final FileScanReport report)
	{
		return report == null ? null : insertAnalyzeScans(element, report.getScans());
	}
	
	
	protected abstract int
	scanElementAndPutReportInDb(final E element);
	
	protected int
	scanElementsAndPutReportInDb(final Collection<E> elements)
	{
		if(elements == null)
		{
			return -1;
		}
		
		int nb = 0;
		for(final E element : elements)
		{
			if(scanElementAndPutReportInDb(element) > 0)
			{
				++nb;
				notifyObservers(element);
			}
		}
		return nb;
	}
	
	protected abstract int
	analyzeSomeUnalizedElementsAndPutReportsInDb(int limit);
	
	protected final int
	analyzeUnalizedElementsAndPutReportsInDb()
	{
		return analyzeSomeUnalizedElementsAndPutReportsInDb(-1);
	}
}
