/*
 * Copyright 2016  Nicola Spanti  <dev@nicola-spanti.info>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * https://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package br.unicamp.ic.lasca.unilea.file;


import java.io.File;
import java.io.IOException;
import java.io.PrintStream;
import java.net.URLConnection;
import java.nio.file.Files;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import br.unicamp.ic.lasca.unilea.DefaultValues;
import br.unicamp.ic.lasca.unilea.utils.CommandLineArgumentsUtils;


public final class FilesAdderToDb
{
	/**
	 * Don't let anyone instantiate this class.
	 */
	private
	FilesAdderToDb()
	{}


	private static Connection connection = null;
	private static final String containsStringRequest =
			"SELECT 'yes' AS result\n"+
					"FROM file\n"+
					"WHERE name     = ?\n"+
					"AND   payload  = ?\n"+
					"AND   mimetype = ?";
	private static final String insertStringRequest =
			"INSERT INTO file (name, payload, mimetype)\n"+
					"VALUES (?, ?, ?)\n"+
					"ON CONFLICT DO NOTHING";
	private static PreparedStatement containsStatement = null;
	private static PreparedStatement insertStatement   = null;
	
	private static final PrintStream defaultErrorStream = System.err;
	
	
	private static synchronized boolean
	init(final String url, final String user, final String password)
	{
		try
		{
			Class.forName("org.postgresql.Driver");
		}
		catch (final ClassNotFoundException e)
		{
			e.printStackTrace();
			return false;
		}
		
		try
		{
			connection = DriverManager.getConnection(
					url, user, password);
		}
		catch (final SQLException e)
		{
			e.printStackTrace();
			return false;
		}
		
		try
		{
			containsStatement =
					connection.prepareStatement(containsStringRequest);
		}
		catch (final SQLException e)
		{
			e.printStackTrace();
			return false;
		}
		
		try
		{
			insertStatement =
					connection.prepareStatement(insertStringRequest);
		}
		catch (final SQLException e)
		{
			e.printStackTrace();
			return false;
		}
		
		return true;
	}
	
	private static boolean
	init()
	{
		return init(
				DefaultValues.dataBaseUrl,
				DefaultValues.dataBaseUser,
				DefaultValues.dataBasePassword
				);
	}
	
	private static synchronized boolean
	destroy()
	{
		if(connection != null)
		{
			try
			{
				connection.close();
			}
			catch (final SQLException e)
			{
				e.printStackTrace();
				return false;
			}
		}
		return true;
	}
	
	
	private static boolean
	contains(final String name, final byte[] payload, final String mimeType)
	{
		if(name == null || name.isEmpty() || payload == null || payload.length == 0)
		{
			return false;
		}
		
		try
		{
			containsStatement.setString(1, name);
			containsStatement.setBytes (2, payload);
			containsStatement.setString(3, mimeType);
			final ResultSet result = containsStatement.executeQuery();
			return result.next();
		}
		catch (final SQLException e)
		{
			e.printStackTrace();
			return false;
		}
	}
	
	private static boolean
	insert(final String name, final byte[] payload, final String mimeType)
	{
		if(
				name == null || name.isEmpty() ||
				payload == null || payload.length == 0 ||
				contains(name, payload, mimeType))
		{
			return false;
		}
		
		try
		{
			insertStatement.setString(1, name);
			insertStatement.setBytes (2, payload);
			insertStatement.setString(3, mimeType);
			insertStatement.executeUpdate();
		}
		catch (final SQLException e)
		{
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
	private static boolean
	insert(final File file, final PrintStream errorWriter)
	{
		if(file == null)
		{
			if(errorWriter != null)
			{
				errorWriter.println("file is null.");
				errorWriter.flush();
			}
			return false;
		}
		if(!file.exists())
		{
			if(errorWriter != null)
			{
				errorWriter.println("File "+ file.getAbsolutePath() +" does not exist.");
				errorWriter.flush();
			}
			return false;
		}
		if(file.isDirectory())
		{
			if(errorWriter != null)
			{
				errorWriter.println(file.getAbsolutePath() +" is a directory.");
				errorWriter.println("Directories are not managed.");
				errorWriter.flush();
			}
			return false;
		}
		if(!file.canRead())
		{
			if(errorWriter != null)
			{
				errorWriter.println(
						"File "+ file.getAbsolutePath() +" can not be read.");
				errorWriter.flush();
			}
			return false;
		}
		
		final String fileName = file.getName();
		byte[] payload;
		try
		{
			payload = Files.readAllBytes(file.toPath());
		}
		catch (IOException e)
		{
			e.printStackTrace();
			return false;
		}
		final String mimeType = URLConnection.guessContentTypeFromName(fileName);
		return insert(fileName, payload, mimeType);
	}
	
	private static boolean
	insert(final File file, boolean printError)
	{
		return insert(file, printError ? defaultErrorStream : null);
	}
	
	@SuppressWarnings("unused")
	private static boolean
	insert(final File file)
	{
		return insert(file, true);
	}
	
	private static boolean
	insert(final String filePath, final PrintStream errorWriter)
	{
		return insert(new File(filePath), errorWriter);
	}
	
	private static boolean
	insert(final String filePath, boolean printError)
	{
		return insert(new File(filePath), printError ? defaultErrorStream : null);
	}
	
	@SuppressWarnings("unused")
	private static boolean
	insert(final String filePath)
	{
		return insert(filePath, true);
	}
	
	private static int
	insert(final String[] files, final PrintStream errorWriter)
	{
		if(files == null)
		{
			return 0;
		}
		
		int nb = 0;
		for(final String file : files)
		{
			if(insert(file, errorWriter))
			{
				++nb;
			}
		}
		return nb;
	}
	
	private static int
	insert(final String[] files, boolean printError)
	{
		return insert(files, printError ? System.err : null);
	}
	
	private static int
	insert(final String[] files)
	{
		return insert(files, true);
	}
	
	
	private static void
	printHelp(final PrintStream stream)
	{
		if(stream != null)
		{
			stream.println("Give paths of files to add them to the database.");
		}
	}
	
	private static void
	printHelp()
	{
		printHelp(System.out);
	}
	
	public static void
	main(final String[] args)
	{
		if(args.length != 1)
		{
			printHelp(System.err);
			System.exit(1);
		}
		if(CommandLineArgumentsUtils.isHelpArgument(args[0]))
		{
			printHelp();
			System.exit(0);
		}
		
		if(!init())
		{
			System.err.println("Initialization failed!");
			System.exit(1);
		}
		
		int nb = insert(args);
		destroy();
		
		if(nb < 0)
		{
			System.err.println("An error occured.");
		}
		else if(nb < 2)
		{
			System.out.println(nb +" file putted.");
		}
		else
		{
			System.out.println(nb +" files putted.");
		}
	}
}
