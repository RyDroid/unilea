/*
 * Copying and distribution of this file, with or without modification,
 * are permitted in any medium without royalty provided this notice is
 * preserved.  This file is offered as-is, without any warranty.
 */

package br.unicamp.ic.lasca.unilea.utils;

public enum WaiterObserverEventType
{
	BEFORE,
	AFTER
}
