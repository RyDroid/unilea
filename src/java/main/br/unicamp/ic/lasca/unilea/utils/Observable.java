/*
 * Copying and distribution of this file, with or without modification,
 * are permitted in any medium without royalty provided this notice is
 * preserved.  This file is offered as-is, without any warranty.
 * Names of contributors must not be used to endorse or promote products
 * derived from this file without specific prior written permission.
 */


package br.unicamp.ic.lasca.unilea.utils;


public interface Observable
{
	public boolean
	addObserver(final Observer observer);
	
	public boolean
	removeObserver(final Observer observer);
	
	public void
	removeObservers();
	
	public int
	countObservers();
	
	public int
	notifyObservers(final Object data);
}
