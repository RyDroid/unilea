/*
 * Copyright 2016  Nicola Spanti  <dev@nicola-spanti.info>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * https://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package br.unicamp.ic.lasca.unilea.email.score;


import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;

import br.unicamp.ic.lasca.unilea.genericscore.ScoreManagerDbEventType;
import br.unicamp.ic.lasca.unilea.genericscore.SecurityScoreAlgorithmDb;
import br.unicamp.ic.lasca.unilea.utils.Observable;
import br.unicamp.ic.lasca.unilea.utils.ObservableUtils;
import br.unicamp.ic.lasca.unilea.utils.Observer;
import br.unicamp.ic.lasca.unilea.utils.ObserverDataWithIdentifier;


public abstract class EmailScoreManagerDbDefault
implements EmailScoreManagerDb, Observable
{
	private Connection _connection = null;
	private final String getWebPagesWithoutScoreStringRequest =
			"SELECT id\n"+
					"FROM email_content\n"+
					"WHERE id NOT IN (\n"+
					"      SELECT email_id\n"+
					"      FROM   email_security_score_int\n"+
					"      WHERE  algorithm_id = ?\n"+
					")\n"+
					"LIMIT ?";
	private PreparedStatement getWebPagesWithoutScoreStatement = null;
	
	private SecurityScoreAlgorithmDb _algorithm = null;
	
	private Collection<Observer> _observers = new ArrayList<Observer>();
	
	
	public EmailScoreManagerDbDefault(
			final String algorithmName,
			final Connection connection)
	{
		_connection = connection;
		_algorithm  = new SecurityScoreAlgorithmDb(algorithmName);
	}
	
	public EmailScoreManagerDbDefault(
			final String algorithmName)
	{
		this(algorithmName, null);
	}
	
	
	public synchronized boolean
	setDataBaseConnexion(final Connection connection)
	{
		if(connection == null)
		{
			return false;
		}
		
		if(getWebPagesWithoutScoreStatement != null)
		{
			try
			{
				getWebPagesWithoutScoreStatement.close();
			}
			catch (SQLException e)
			{
				e.printStackTrace();
				return false;
			}
			getWebPagesWithoutScoreStatement = null;
		}
		_connection = connection;
		return true;
	}
	
	public synchronized boolean
	init()
	{
		if(_connection == null)
		{
			return false;
		}
		
		if(getWebPagesWithoutScoreStatement == null)
		{
			try
			{
				getWebPagesWithoutScoreStatement =
						_connection.prepareStatement(
								getWebPagesWithoutScoreStringRequest);
			}
			catch (final SQLException e)
			{
				e.printStackTrace();
				return false;
			}
		}
		
		_algorithm.setDataBaseConnexion(_connection);
		
		return true;
	}

	@Override
	public synchronized boolean
	destroy()
	{
		if(_connection != null)
		{
			try
			{
				_connection.close();
			}
			catch (final SQLException e)
			{
				e.printStackTrace();
				return false;
			}
		}
		return true;
	}
	
	
	@Override
	public int
	getAlgorithmId()
	{
		return _algorithm == null ? -1 : _algorithm.getId();
	}
	
	
	private Collection<Integer>
	getIdOfWebPagesWithoutScore(final ResultSet results)
	{
		if(results == null)
		{
			return null;
		}
		
		Collection<Integer> ids = new ArrayList<Integer>();
		try
		{
			while(results.next())
			{
				ids.add(results.getInt(1));
			}
			results.close();
		}
		catch (final SQLException e)
		{
			e.printStackTrace();
			return null;
		}
		return ids;
	}

	@Override
	public Collection<Integer>
	getIdOfEmailsWithoutScore(int limit)
	{
		ResultSet webpagesSql;
		try
		{
			getWebPagesWithoutScoreStatement.setInt(1, getAlgorithmId());
			if(limit > 0)
			{
				getWebPagesWithoutScoreStatement.setInt(2, limit);
			}
			else
			{
				getWebPagesWithoutScoreStatement.setInt(2, Integer.MAX_VALUE);
			}
			webpagesSql = getWebPagesWithoutScoreStatement.executeQuery();
		}
		catch (final SQLException e)
		{
			e.printStackTrace();
			return null;
		}
		return getIdOfWebPagesWithoutScore(webpagesSql);
	}

	@Override
	public Collection<Integer>
	getIdOfEmailsWithoutScore()
	{
		return getIdOfEmailsWithoutScore(-1);
	}
	
	
	@Override
	public int
	insertScores(final Collection<Integer> ids)
	{
		if(ids == null)
		{
			return 0;
		}
		
		final int nb_ids = ids.size();
		int nb_done    = 0;
		int nb_success = 0;
		for(int id : ids)
		{
			++nb_done;
			if(insertScore(id))
			{
				++nb_success;
				
				notifyObservers(
						new ObserverDataWithIdentifier(
								ScoreManagerDbEventType.INSERT_SCORES_PROGRESS,
								((float) nb_done) / ((float) nb_ids)
								)
						);
			}
		}
		return nb_success;
	}
	
	
	@Override
	public int
	computeScores(int limit)
	{
		return insertScores(
				getIdOfEmailsWithoutScore(limit)
				);
	}

	@Override
	public int
	computeScores()
	{
		return computeScores(-1);
	}
	
	
	@Override
	public boolean
	addObserver(final Observer observer)
	{
		return _observers.add(observer);
	}
	
	@Override
	public boolean
	removeObserver(final Observer observer)
	{
		return _observers.remove(observer);
	}
	
	@Override
	public void
	removeObservers()
	{
		_observers.clear();
	}
	
	@Override
	public int
	countObservers()
	{
		return _observers.size();
	}
	
	@Override
	public int
	notifyObservers(final Object argument)
	{
		return ObservableUtils.notifyObserversFromIterable(
				this, _observers, argument
				);
	}
	
	protected void
	notifyScoreInserted()
	{
		notifyObservers(
				new ObserverDataWithIdentifier(
						ScoreManagerDbEventType.SCORE_INSERTED
						)
				);
	}
}
