/*
 * Copyright 2016  Nicola Spanti  <dev@nicola-spanti.info>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * https://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package br.unicamp.ic.lasca.unilea.file.analyze;


import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;

import br.unicamp.ic.lasca.unilea.file.FileDb;
import br.unicamp.ic.lasca.unilea.genericanalyze.AnalyzeKeyValueInterface;
import br.unicamp.ic.lasca.unilea.genericanalyze.DataBaseGenericAnalyzeKeyValueManager;


public class DataBaseFileAnalyzeKeyValueManager
extends DataBaseGenericAnalyzeKeyValueManager<FileDb>
{
	private Connection connection = null;
	private static final String getUnanlyzedFilesStringRequest =
			"SELECT id, name, encode(payload, 'escape') AS payload\n"+
					"FROM file\n"+
					"WHERE id NOT IN("+
					"      SELECT DISTINCT file_id\n"+
					"      FROM file_analyze_key_value\n"+
					"      INNER JOIN security_analyzer\n"+
					"                 ON security_analyzer.id = analyzer_id\n"+
					"      WHERE security_analyzer.name = ?\n"+
					")\n"+
					"AND payload IS NOT NULL\n"+
					"AND octet_length(payload) > 4\n"+
					"LIMIT ?";
	private static final String insertFileAnalyzeKeyValueStringRequest =
			"INSERT INTO file_analyze_key_value (file_id, analyzer_id, key, value)\n"+
					"VALUES(?, ?, ?, ?)\n"+
					"ON CONFLICT DO NOTHING";
	private PreparedStatement getUnanlyzedFilesStatement          = null;
	private PreparedStatement insertFileAnalyzeKeyValueStatement  = null;
	
	
	public
	DataBaseFileAnalyzeKeyValueManager(
			final Connection aConnection,
			final String theAnalyzerName)
	{
		super(aConnection, theAnalyzerName);
		connection = aConnection;
	}
	
	
	@Override
	public boolean
	init()
	{
		if(!super.init())
		{
			return false;
		}
		
		if(getUnanlyzedFilesStatement == null)
		{
			try
			{
				getUnanlyzedFilesStatement =
						connection.prepareStatement(
								getUnanlyzedFilesStringRequest);
			}
			catch (final SQLException e)
			{
				e.printStackTrace();
				return false;
			}
		}

		if(insertFileAnalyzeKeyValueStatement == null)
		{
			try
			{
				insertFileAnalyzeKeyValueStatement =
						connection.prepareStatement(
								insertFileAnalyzeKeyValueStringRequest);
			}
			catch (final SQLException e)
			{
				e.printStackTrace();
				return false;
			}
		}

		return true;
	}
	
	@Override
	public boolean
	commit()
	{
		if(connection == null)
		{
			return false;
		}
		
		try
		{
			if(!connection.getAutoCommit())
			{
				connection.commit();
			}
			return true;
		}
		catch (final Exception e)
		{
			e.printStackTrace();
			return false;
		}
	}

	@Override
	public boolean
	destroy()
	{
		if(connection != null)
		{
			commit();
			try
			{
				connection.close();
			}
			catch (final SQLException e)
			{
				e.printStackTrace();
				return false;
			}
			connection = null;
		}
		return super.destroy();
	}
	
	
	public static Collection<FileDb>
	getUnalizedFiles(final ResultSet results)
	{
		if(results == null)
		{
			return null;
		}
		
		final Collection<FileDb> files = new ArrayList<FileDb>();
		try
		{
			while(results.next())
			{
				files.add(new FileDb(results));
			}
			results.close();
		}
		catch (final SQLException e)
		{
			e.printStackTrace();
			return null;
		}
		return files;
	}
	
	@Override
	public Collection<FileDb>
	getSomeUnalizedElements(int limit)
	{
		ResultSet results = null;
		try
		{
			getUnanlyzedFilesStatement.setString(1, getAnalyzerName());
			if(limit > 0)
			{
				getUnanlyzedFilesStatement.setInt(2, limit);
			}
			else
			{
				getUnanlyzedFilesStatement.setInt(2, Integer.MAX_VALUE);
			}
			results = getUnanlyzedFilesStatement.executeQuery();
		}
		catch (final SQLException e)
		{
			e.printStackTrace();
			return null;
		}
		return getUnalizedFiles(results);
	}
	
	
	public boolean
	insertAnalyzeKeyValue(
			int file_id,
			final String key,
			final String value)
	{
		if(
				file_id < 0   ||
				key   == null || key.isEmpty() || 
				value == null || value.isEmpty())
		{
			return false;
		}
		
		try
		{
			insertFileAnalyzeKeyValueStatement.setInt   (1, file_id);
			insertFileAnalyzeKeyValueStatement.setInt   (2, getAnalyzerIdOfDb());
			insertFileAnalyzeKeyValueStatement.setString(3, key);
			insertFileAnalyzeKeyValueStatement.setString(4, value);
			insertFileAnalyzeKeyValueStatement.executeUpdate();
		}
		catch (final SQLException e)
		{
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
	public boolean
	insertAnalyzeKeyValue(
			int file_id,
			final String key,
			final String value,
			final String valueForNull)
	{
		return insertAnalyzeKeyValue(
				file_id, key,
				value == null ? valueForNull : value);
	}
	
	public boolean
	insertAnalyzeKeyValue(
			final FileDb file,
			final String key,
			final String value,
			final String valueForNull)
	{
		return insertAnalyzeKeyValue(file.getId(), key, value);
	}
	
	public boolean
	insertAnalyzeKeyValue(
			final FileDb file,
			final String key,
			final String value)
	{
		return insertAnalyzeKeyValue(file, key, value, null);
	}
	
	public boolean
	insertAnalyzeKeyValue(
			final FileDb file,
			final AnalyzeKeyValueInterface keyValue,
			final String valueForNull)
	{
		if(keyValue == null)
		{
			return false;
		}
		return insertAnalyzeKeyValue(
				file,
				keyValue.getKey(), keyValue.getValue(),
				valueForNull);
	}
	
	@Override
	public boolean
	insertAnalyzeKeyValue(
			final FileDb file,
			final AnalyzeKeyValueInterface keyValue)
	{
		return insertAnalyzeKeyValue(file, keyValue, null);
	}
}
