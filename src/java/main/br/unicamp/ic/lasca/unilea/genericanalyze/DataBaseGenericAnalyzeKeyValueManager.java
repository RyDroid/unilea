/*
 * Copyright 2016  Nicola Spanti  <dev@nicola-spanti.info>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * https://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package br.unicamp.ic.lasca.unilea.genericanalyze;


import java.sql.Connection;
import java.util.Collection;

import br.unicamp.ic.lasca.unilea.utils.Commitable;
import br.unicamp.ic.lasca.unilea.utils.ObservableDefault;


public abstract class DataBaseGenericAnalyzeKeyValueManager<E>
extends ObservableDefault
implements Commitable
{
	private AnalyzerDb _analyzer;
	
	
	public
	DataBaseGenericAnalyzeKeyValueManager(
			final Connection connection,
			final String     analyzerName)
	{
		if(connection == null || analyzerName == null || analyzerName.isEmpty())
		{
			_analyzer = null;
		}
		else
		{
			_analyzer = new AnalyzerDb(connection, analyzerName);
		}
	}
	
	
	public boolean
	init()
	{
		return true;
	}
	
	public boolean
	destroy()
	{
		return true;
	}

	
	public final boolean
	hasValidAnalyzer()
	{
		return _analyzer != null;
	}
	
	public final boolean
	insertAnalyzerInDb()
	{
		return _analyzer == null ? false : _analyzer.insert();
	}
	
	public final int
	getAnalyzerIdOfDb()
	{
		return _analyzer == null ? -1 : _analyzer.getId();
	}
	
	public final String
	getAnalyzerName()
	{
		return _analyzer == null ? null : _analyzer.getName();
	}
	
	
	public abstract Collection<E>
	getSomeUnalizedElements(int limit);
	
	public final Collection<E>
	getUnalizedElements()
	{
		return getSomeUnalizedElements(-1);
	}
	
	public int
	getCountUnalizedElements()
	{
		final Collection<E> elements = getUnalizedElements();
		return elements == null ? -1 : elements.size();
	}
	
	
	public abstract boolean
	insertAnalyzeKeyValue(
			final E element,
			final AnalyzeKeyValueInterface keyValue);
	
	public int
	insertCollectionAnalyzeKeyValue(
			final E element,
			final Collection<AnalyzeKeyValueInterface> keysAndValues)
	{
		if(keysAndValues == null)
		{
			return 0;
		}
		
		int nb = 0;
		for(final AnalyzeKeyValueInterface keyAndValue : keysAndValues)
		{
			if(insertAnalyzeKeyValue(element, keyAndValue))
			{
				++nb;
			}
		}
		return nb;
	}
}
