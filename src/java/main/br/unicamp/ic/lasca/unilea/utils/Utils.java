/*
 * Copying and distribution of this file, with or without modification,
 * are permitted in any medium without royalty provided this notice is
 * preserved.  This file is offered as-is, without any warranty.
 * Names of contributors must not be used to endorse or promote products
 * derived from this file without specific prior written permission.
 */


package br.unicamp.ic.lasca.unilea.utils;


public final class Utils
{
	/**
	 * Don't let anyone instantiate this class.
	 */
	private
	Utils()
	{}
	
	
	public static boolean
	sleep(long ms)
	{
		try
		{
			Thread.sleep(ms);
		}
		catch (final InterruptedException e)
		{
			e.printStackTrace();
			return false;
		}
		return true;
	}
}
