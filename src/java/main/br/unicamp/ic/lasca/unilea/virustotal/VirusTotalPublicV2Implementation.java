/*
 * Copyright 2016  Nicola Spanti  <dev@nicola-spanti.info>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * https://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package br.unicamp.ic.lasca.unilea.virustotal;


import java.io.IOException;

import com.kanishka.virustotal.dto.FileScanReport;
import com.kanishka.virustotal.exception.APIKeyNotFoundException;
import com.kanishka.virustotal.exception.InvalidArguentsException;
import com.kanishka.virustotal.exception.QuotaExceededException;
import com.kanishka.virustotal.exception.UnauthorizedAccessException;
import com.kanishka.virustotalv2.VirustotalPublicV2Impl;


public class VirusTotalPublicV2Implementation
extends VirustotalPublicV2Impl
implements VirusTotalPublicV2Interface
{
	public VirusTotalPublicV2Implementation()
	throws APIKeyNotFoundException
	{
		super();
	}
	
	
	public FileScanReport
	getUrlScanReport(final String url, boolean scan)
	throws IOException, UnauthorizedAccessException, QuotaExceededException, InvalidArguentsException
	{
		final String[] urls = { url };
		FileScanReport[] result = getUrlScanReport(urls, scan);
		return result == null || result.length == 0 ? null : result[0];
	}
}
