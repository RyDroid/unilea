/*
 * Copyright 2016  Nicola Spanti  <dev@nicola-spanti.info>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * https://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package br.unicamp.ic.lasca.unilea.webpage;


import java.sql.ResultSet;
import java.sql.SQLException;


public class WebPageDb
{
	private int    _id;
	private String _url;
	
	
	public WebPageDb(int id, final String url)
	{
		_id  =  id;
		_url = url;
	}
	
	public WebPageDb(final ResultSet res) throws SQLException
	{
		this(res.getInt("id"), res.getString("url"));
	}

	
	public boolean
	hasValidId()
	{
		return _id >= 0;
	}
	
	public int
	getId()
	{
		return _id;
	}
	
	public String
	getUrl()
	{
		return _url;
	}
}
