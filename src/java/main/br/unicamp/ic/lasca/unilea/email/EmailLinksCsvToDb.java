/*
 * Copyright 2016  Nicola Spanti  <dev@nicola-spanti.info>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * https://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package br.unicamp.ic.lasca.unilea.email;


import java.io.PrintStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Set;

import org.apache.commons.csv.CSVRecord;

import br.unicamp.ic.lasca.unilea.DefaultValues;
import br.unicamp.ic.lasca.unilea.utils.ApacheCommonsCsvUtils;
import br.unicamp.ic.lasca.unilea.utils.CommandLineArgumentsUtils;
import br.unicamp.ic.lasca.unilea.utils.UrlUtils;
import br.unicamp.ic.lasca.unilea.webpage.DataBaseWebPageManager;


public final class EmailLinksCsvToDb
{
	/**
	 * Don't let anyone instantiate this class.
	 */
	private
	EmailLinksCsvToDb()
	{}
	

	private static Connection connection = null;
	private static final String insertEmailLinkStringRequest =
			"INSERT INTO email_link VALUES (?, ?) ON CONFLICT DO NOTHING";
	private static PreparedStatement insertEmailLinkStatement = null;
	private static DataBaseWebPageManager webPageManager = null;
	
	
	private static synchronized boolean
	init(final String url, final String user, final String password)
	{
		try
		{
			Class.forName("org.postgresql.Driver");
		}
		catch (final ClassNotFoundException e)
		{
			e.printStackTrace();
			return false;
		}
		
		try
		{
			connection = DriverManager.getConnection(
					url, user, password);
		}
		catch (final SQLException e)
		{
			e.printStackTrace();
			return false;
		}
		
		try
		{
			insertEmailLinkStatement =
					connection.prepareStatement(insertEmailLinkStringRequest);
		}
		catch (final SQLException e)
		{
			e.printStackTrace();
			return false;
		}
		
		webPageManager = new DataBaseWebPageManager(connection);
		
		return true;
	}
	
	private static boolean
	init()
	{
		return init(
				DefaultValues.dataBaseUrl,
				DefaultValues.dataBaseUser,
				DefaultValues.dataBasePassword
				);
	}
	
	public static synchronized boolean
	commit()
	{
		if(connection == null)
		{
			return false;
		}
		
		try
		{
			if(!connection.getAutoCommit())
			{
				connection.commit();
			}
			return true;
		}
		catch (final Exception e)
		{
			e.printStackTrace();
			return false;
		}
	}
	
	private static synchronized boolean
	destroy()
	{
		if(connection != null)
		{
			commit();
			try
			{
				connection.close();
			}
			catch (final SQLException e)
			{
				e.printStackTrace();
				return false;
			}
		}
		return true;
	}
	
	
	private static boolean
	putWebPageInDataBase(final String url)
	{
		return webPageManager.insert(url);
	}
	
	private static String
	putWebPageInDataBase(
			final CSVRecord record,
			final String column)
	{
		if(!record.isMapped(column))
		{
			return null;
		}
		final String url = record.get(column);
		
		if(url == null || url.isEmpty() || !putWebPageInDataBase(url))
		{
			return null;
		}
		return url;
	}
	
	public static int
	getIdEmailFromRecord(final CSVRecord record)
	{
		if(record == null || !record.isMapped("idemail"))
		{
			return -1;
		}
		try
		{
			return Integer.parseInt(record.get("idemail"));
		}
		catch(final NumberFormatException e)
		{
			return -1;
		}
	}
	
	public static boolean
	insertLink(int idEmail, final String urlString)
	{
		if(!UrlUtils.isWeb(urlString))
		{
			return false;
		}
		
		int idWebPage = webPageManager.insertAndGetId(urlString);
		if(idWebPage < 0)
		{
			return false;
		}
		
		try
		{
			insertEmailLinkStatement.setInt(1, idEmail);
			insertEmailLinkStatement.setInt(2, idWebPage);
			insertEmailLinkStatement.execute();
		}
		catch (final SQLException e)
		{
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
	public static int
	insertLinks(int idEmail, final Set<String> links)
	{
		int nb = 0;
		for(final String urlString : links)
		{
			if(insertLink(idEmail, urlString))
			{
				++nb;
			}
		}
		return nb;
	}
	
	private static boolean
	putRecordInDataBase(final CSVRecord record)
	{
		int idEmail = getIdEmailFromRecord(record);
		final String url = putWebPageInDataBase(record, "link");
		
		if(idEmail < 0 || url == null || url.isEmpty())
		{
			return false;
		}
		
		return insertLink(idEmail, url);
	}
	
	private static int
	putRecordsInDataBase(final Iterable<CSVRecord> records)
	{
		int nb = 0;
		for (final CSVRecord record : records)
		{
			if(putRecordInDataBase(record))
			{
				++nb;
			}
		}
		return nb;
	}
	
	
	public static void
	printHelp(final PrintStream stream)
	{
		if(stream != null)
		{
			stream.println("Only 1 argument needed: a CSV file path");
		}
	}

	public static void
	printHelp()
	{
		printHelp(System.out);
	}
	
	public static void
	main(final String[] args)
	{
		if(args.length != 1)
		{
			printHelp(System.err);
			System.exit(1);
		}
		if(CommandLineArgumentsUtils.isHelpArgument(args[0]))
		{
			printHelp();
			System.exit(0);
		}
		
		final Iterable<CSVRecord> records =
				ApacheCommonsCsvUtils.getRecordsWithDefaultFormat(args[0]);
		if(records == null)
		{
			System.err.println(args[0] +" is not a file or a valid CSV.");
			System.exit(1);
		}
		
		if(!init())
		{
			System.err.println("Initialization failed!");
			System.exit(1);
		}
		
		int nb = putRecordsInDataBase(records);
		destroy();
		
		if(nb < 0)
		{
			System.err.println("Something wrong happened!");
			System.exit(1);
		}
		if(nb == 0)
		{
			System.out.println("No record putted");
		}
		else if(nb == 1)
		{
			System.out.println("1 record putted");
		}
		else
		{
			System.out.println(nb +" records putted");
		}
	}
}
