/*
 * Copyright 2016  Nicola Spanti  <dev@nicola-spanti.info>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * https://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package br.unicamp.ic.lasca.unilea.email;


import java.io.PrintStream;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.Set;

import br.unicamp.ic.lasca.unilea.DefaultValues;
import br.unicamp.ic.lasca.unilea.utils.CommandLineArgumentsUtils;


public final class EmailInDbWithoutLinksToCsvLinks
{
	/**
	 * Don't let anyone instantiate this class.
	 */
	private
	EmailInDbWithoutLinksToCsvLinks()
	{}
	
	
	private static Connection connection = null;
	private static final String getContentOfEmailsWithoutLinkStringRequest =
			"SELECT id, plain_text, html\n"+
					"FROM email_content\n" +
					"WHERE id NOT IN (SELECT email_id FROM email_link)";
	private static PreparedStatement getContentOfEmailsWithoutLinkStatement = null;
	
	private static final String DEFAULT_CSV_FILE_PATH = "emails.csv";
	
	
	private static synchronized boolean
	init(final String url, final String user, final String password)
	{
		try
		{
			Class.forName("org.postgresql.Driver");
		}
		catch (final ClassNotFoundException e)
		{
			e.printStackTrace();
			return false;
		}
		
		try
		{
			connection = DriverManager.getConnection(
					url, user, password);
		}
		catch (final SQLException e)
		{
			e.printStackTrace();
			return false;
		}
		
		try
		{
			getContentOfEmailsWithoutLinkStatement =
					connection.prepareStatement(
							getContentOfEmailsWithoutLinkStringRequest);
		}
		catch (final SQLException e)
		{
			e.printStackTrace();
			return false;
		}
		
		return true;
	}
	
	private static boolean
	init()
	{
		return init(
				DefaultValues.dataBaseUrl,
				DefaultValues.dataBaseUser,
				DefaultValues.dataBasePassword
				);
	}
	
	private static synchronized boolean
	destroy()
	{
		if(connection != null)
		{
			try
			{
				connection.close();
			}
			catch (final SQLException e)
			{
				e.printStackTrace();
				return false;
			}
		}
		return true;
	}

	
	private static ResultSet
	getEmailsContentWithoutLinkSql()
	{
		try
		{
			return getContentOfEmailsWithoutLinkStatement.executeQuery();
		}
		catch (final SQLException e)
		{
			e.printStackTrace();
			return null;
		}
	}
	
	private static Collection<EmailContent>
	getEmailsContentWithoutLink()
	{
		final ResultSet emailsSql = getEmailsContentWithoutLinkSql();
		if(emailsSql == null)
		{
			return null;
		}
		
		final Collection<EmailContent> emails = new ArrayList<EmailContent>();
		try
		{
			while(emailsSql.next())
			{
				emails.add(new EmailContent(
						emailsSql.getString("plain_text"),
						emailsSql.getString("html"),
						emailsSql.getInt("id"))
				);
			}
			emailsSql.close();
		}
		catch (final SQLException e)
		{
			e.printStackTrace();
			return null;
		}
		return emails;
	}
	
	public static int
	printLinksInCsvOfEmailsWithoutKnownLinks(
			final String fileName,
			boolean verboseError)
	{
		final Collection<EmailContent> emails = getEmailsContentWithoutLink();
		
		final Collection<String> htmlPages =
				EmailHtmlUtils.getHtmlAsPlainTextFromEmailsContent(emails);
		final Collection<Set<String>> linksPerEmail =
				EmailHtmlUtils.getStringLinksFromPlainText(htmlPages);
		if(htmlPages == null || linksPerEmail == null)
		{
			if(verboseError)
			{
				System.err.println("htmlPages == null || linksPerEmail == null");
			}
			return -1;
		}
		
		final PrintWriter csvWriter =
				EmailLinksCsvUtils.getWriterWithHeader(fileName);
		if(csvWriter == null)
		{
			if(verboseError)
			{
				System.err.println("It is impossible to write to "+ fileName);
			}
			return -1;
		}
		
		int nb = 0;
		final Iterator<EmailContent> it1 = emails.iterator();
		final Iterator<Set<String>>  it2 = linksPerEmail.iterator();
		while(it1.hasNext() && it2.hasNext())
		{
			final int idEmail = it1.next().getId();
			final Set<String> linksOfCurrentEmail = it2.next();
			int tmp = EmailLinksCsvUtils.putLinks(csvWriter, idEmail, linksOfCurrentEmail);
			
			if(tmp >= 0)
			{
				nb += tmp;
			}
			else if(verboseError)
			{
				System.err.println("Problem(s) with email_id="+ idEmail);
			}
		}
		
		if(verboseError)
		{
			System.err.flush();
		}
		csvWriter.flush();
		csvWriter.close();
		return nb;
	}
	
	public static int
	printLinksInCsvOfEmailsWithoutKnownLinks(
			final String fileName)
	{
		return printLinksInCsvOfEmailsWithoutKnownLinks(fileName, true);
	}

	
	public static void
	printHelpError(final PrintStream stream)
	{
		if(stream != null)
		{
			stream.println("One optionnal argument: the file path of the CSV file");
			stream.println("(default = "+ DEFAULT_CSV_FILE_PATH +")");
		}
	}

	public static void
	printHelp(final PrintStream stream)
	{
		if(stream != null)
		{
			stream.println("This program looks for all emails without links in the database.");
			stream.println("It fetchs this emails.");
			stream.println("Then it parses this email to find links.");
			stream.println("Finally, it prints the links with the email id in a CSV.");

			stream.println("");
			printHelpError(stream);
		}
	}
	
	public static void
	printHelp()
	{
		printHelp(System.out);
	}
	
	public static void
	main(String[] args)
	{
		if(args.length > 0 && CommandLineArgumentsUtils.isHelpArgument(args[0]))
		{
			printHelp();
			System.exit(0);
		}
		
		String csvFilePath;
		if(args.length > 0 && !args[0].isEmpty())
		{
			csvFilePath = args[0];
		}
		else
		{
			csvFilePath = DEFAULT_CSV_FILE_PATH;
		}
		
		if(!init())
		{
			System.err.println("Initialization failed!");
			System.exit(1);
		}
		
		int nb = printLinksInCsvOfEmailsWithoutKnownLinks(csvFilePath);
		destroy();
		
		if(nb < 0)
		{
			System.err.println("Something wrong happened!");
			System.exit(1);
		}
		if(nb == 0)
		{
			System.out.println("No link found");
		}
		else if(nb == 1)
		{
			System.out.println("1 link found");
		}
		else
		{
			System.out.println(nb +" links found");
		}
	}
}
