/*
 * Copying and distribution of this file, with or without modification,
 * are permitted in any medium without royalty provided this notice is
 * preserved.  This file is offered as-is, without any warranty.
 * Names of contributors must not be used to endorse or promote products
 * derived from this file without specific prior written permission.
 */


package br.unicamp.ic.lasca.unilea.utils;


public interface Observer
{
	/**
	 * This method is called when an event happened.
	 * @param observable
	 * @param argument
	 * @return True if it managed the event, otherwise false.
	 */
	public boolean
	update(final Observable observable, final Object argument);
}
