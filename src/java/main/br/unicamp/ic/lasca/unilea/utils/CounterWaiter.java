/*
 * Copyright 2016  Nicola Spanti  <dev@nicola-spanti.info>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * https://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package br.unicamp.ic.lasca.unilea.utils;


import java.util.Date;


public class CounterWaiter
extends ObservableDefault
{
	private int  _counterValue = 0;
	private int  _counterLimit = 1;
	private long _counterStartTimestamp = (new Date()).getTime();
	private long _timeToWaitMs = 1;
	
	
	public CounterWaiter(int counterLimit, long timeToWaitMs)
	{
		_counterLimit = counterLimit;
		_timeToWaitMs = timeToWaitMs;
	}
	
	public boolean
	waitMaximum()
	{
		_counterValue = 0;
		return Utils.sleep(_timeToWaitMs);
	}
	
	private long
	getDiff()
	{
		return (new Date()).getTime() - _counterStartTimestamp;
	}
	
	private boolean
	hasToSleep()
	{
		return getDiff() < _timeToWaitMs;
	}
	
	public synchronized boolean
	waitIfNeeded()
	{
		if(_counterValue >= _counterLimit)
		{
			if(hasToSleep())
			{
				notifyObservers(WaiterObserverEventType.BEFORE);
				long diff = getDiff();
				if(diff > _timeToWaitMs)
				{
					diff = _timeToWaitMs;
				}
				boolean hasSleep = Utils.sleep(diff);
				notifyObservers(WaiterObserverEventType.AFTER);
				if(!hasSleep)
				{
					return false;
				}
			}
			_counterStartTimestamp = (new Date()).getTime();
			_counterValue = 0;
		}
		return true;
	}
	
	public synchronized void
	increment()
	{
		++_counterValue;
	}
}
