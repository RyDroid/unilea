/*
 * Copyright 2016  Nicola Spanti  <dev@nicola-spanti.info>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * https://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package br.unicamp.ic.lasca.unilea.genericscore;


import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;


public class SecurityScoreAlgorithmDb
{
	private Connection _connection = null;
	private String     _name       = null;
	private int        _id         = -1;

	
	public SecurityScoreAlgorithmDb(
			final Connection connection,
			final String name)
	{
		_connection = connection;
		_name       = name;
	}
	
	public SecurityScoreAlgorithmDb(final String name)
	{
		_name       = name;
	}
	
	
	public void
	setDataBaseConnexion(final Connection connection)
	{
		_connection = connection;
	}
	
	
	public boolean
	insert()
	{
		if(_id >= 0)
		{
			return true;
		}
		if(_name == null || _name.isEmpty())
		{
			return false;
		}
		
		PreparedStatement stmt = null;
		try
		{
			stmt = _connection.prepareStatement(
					"INSERT INTO security_score_algorithm (name)\n"+
							"VALUES(?)\n"+
							"ON CONFLICT DO NOTHING"
					);
			stmt.setString(1, _name);
			stmt.executeUpdate();
			stmt.close();
		}
		catch (final SQLException e)
		{
			e.printStackTrace();
			return false;
		}
		
		return true;
	}
	
	public int
	getId(boolean insertIfNeeded)
	{
		if(_id < 0)
		{
			if(_connection == null || !isValidName())
			{
				return -1;
			}
			
			PreparedStatement stmt = null;
			try
			{
				stmt = _connection.prepareStatement(
						"SELECT id\n"+
								"FROM security_score_algorithm\n"+
								"WHERE LOWER(name) = ?"
						);
				stmt.setString(1, _name);
				final ResultSet result = stmt.executeQuery();
				if(result.next())
				{
					_id = result.getInt("id");
					stmt.close();
				}
				else if(insertIfNeeded)
				{
					stmt.close();
					if(insert())
					{
						return getId(false);
					}
				}
			}
			catch (final SQLException e)
			{
				e.printStackTrace();
				return -1;
			}
		}
		
		return _id;
	}
	
	public int
	getId()
	{
		return getId(true);
	}
	
	
	public boolean
	isValidName()
	{
		return _name != null && !_name.isEmpty();
	}
	
	public String
	getName()
	{
		return _name;
	}
}
