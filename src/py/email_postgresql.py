# License Creative Commons 0 (like the public domain)
# https://creativecommons.org/publicdomain/zero/1.0/
# Use, study, modify and share!
# Even if you are not forced with copyleft, please respect freedom of others.
# For more informations: https://www.gnu.org/philosophy/free-sw.html
# This file is offered as-is, without any warranty.


import psycopg2
# http://initd.org/psycopg/


def getEmailIdOfDataBase(cursor, message_text, message_html):
    if (cursor is None or
        (message_text == message_html and message_text is None)):
        return -1
    
    cursor.execute("SELECT id FROM email_content "+
                   "WHERE plain_text = %s AND html = %s;",
                   (message_text, message_html))
    result = cursor.fetchone()
    if result is None or len(result) == 0:
        return -1
    return int(result[0])

def putEmailAddressInDataBase(cursor, address):
    if cursor is None or address is None:
        return False
    
    cursor.execute("INSERT INTO email_address (value)\n"+
                   "VALUES (%s)\n"+
                   "ON CONFLICT DO NOTHING;",
                   (address,))
    return True

def putEmailAddressAliasInDataBase(cursor, address, alias):
    if cursor is None or address is None:
        return False

    putEmailAddressInDataBase(cursor, address)
    cursor.execute("INSERT INTO email_address_alias (address_id, value)\n"+
                   "SELECT id, %s AS alias FROM email_address WHERE value = %s\n"+
                   "ON CONFLICT DO NOTHING;",
                   (address, alias))
    return True
