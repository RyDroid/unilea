# License Creative Commons 0 (like the public domain)
# https://creativecommons.org/publicdomain/zero/1.0/
# Use, study, modify and share!
# Even if you are not forced with copyleft, please respect freedom of others.
# For more informations: https://www.gnu.org/philosophy/free-sw.html
# This file is offered as-is, without any warranty.


import pyzmail
# http://www.magiksys.net/pyzmail/


def getDecodedPayloadOfMessage(message):
    if message is not None:
        message = message.get_payload().decode(message.charset)
    return message

def isMailPartAttachment(mailpart):
    return (
        mailpart is not None and
        mailpart.disposition == "attachment" and
        mailpart.filename is not None and
        mailpart.filename != ""
    )

def getAttachmentsOfMessage(message):
    if message is None:
        return None

    attachments = []
    for mailpart in message.mailparts:
        if (isMailPartAttachment(mailpart) and
            mailpart != message.text_part and
            mailpart != message.html_part):
            attachments.append(mailpart)
    return attachments
