# License Creative Commons 0 (like the public domain)
# https://creativecommons.org/publicdomain/zero/1.0/
# Use, study, modify and share!
# Even if you are not forced with copyleft, please respect freedom of others.
# For more informations: https://www.gnu.org/philosophy/free-sw.html
# This file is offered as-is, without any warranty.


from getpass import getpass
import imapclient # https://imapclient.readthedocs.io/


def getImapObjectByAskingToUser(hostname="", useTls=None):
    while hostname is None or hostname == "":
        hostname = raw_input("Hostname IMAP: ")

    if not isinstance(useTls, bool):
        useTls = raw_input("Use TLS? [Y/n] ").lower()
        if useTls == "n" or useTls == "no" or useTls == "false":
            useTls = False
            print("Your email provider sucks.")
        else:
            useTls = True
    
    return imapclient.IMAPClient(hostname, ssl=useTls)

def askEmailPasswordOnce(hideInput):
    if hideInput:
        return getpass("Password email: ")
    return raw_input("Password email: ")

def loginToImapByAskingToUser(imapObject,
                              emailAddress = "",
                              password     = "",
                              hidePassInput=True):
    if imapObject is None:
        return None
    
    while emailAddress is None or emailAddress == "":
        emailAddress = raw_input("Email address: ")
    while password     is None or password     == "":
        password     = askEmailPasswordOnce(hidePassInput)
    return imapObject.login(emailAddress, password)

def getImapObjectConnectedByAskingToUser(hostname     = "",
                                         useTls       = None,
                                         emailAddress = "",
                                         password     = ""):
    imapObject = getImapObjectByAskingToUser(hostname, useTls)
    loginToImapByAskingToUser(imapObject, emailAddress, password)
    return imapObject
