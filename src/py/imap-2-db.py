#!/usr/bin/env python2


# License Creative Commons 0 (like the public domain)
# https://creativecommons.org/publicdomain/zero/1.0/
# Use, study, modify and share!
# Even if you are not forced with copyleft, please respect freedom of others.
# For more informations: https://www.gnu.org/philosophy/free-sw.html
# This file is offered as-is, without any warranty.


import sys
import getopt
from   imap_input      import *
from   imap_postgresql import *

# imapclient and pyzmail can be installed with pip.
# https://pip.pypa.io/
# psycopg2 is available in Debian.
# https://packages.debian.org/jessie/python-psycopg2


def getCommandLineUsage():
    return sys.argv[0] +"\n"+ \
        "\t[? | -? | help | --help]\n" + \
        "\t[-h database_hostname]\n" + \
        "\t[--db-name=database_name]\n" + \
        "\t[-u database_username]\n" + \
        "\t[-p database_userpass]\n" + \
        "\t[--email-host=email_hostname]\n" + \
        "\t[--email-address=email_address]\n" + \
        "\t[--email-pass=email_pass]\n" + \
        "\t[--use-tls]\n" + \
        "\t[--hide-pass | --show-pass]\n" + \
        "\t[--dbg | --debug]"


if __name__ == "__main__":
    dbHostname    = "localhost"
    dbName        = "unilea"
    dbUserName    = "postgres"
    dbUserPass    = "postgres"
    emailHostname = ""
    emailAddress  = ""
    emailPass     = ""
    useTls        = None
    hidePassInput = True
    debug         = False
    
    try:
      opts, args = getopt.getopt(sys.argv[1:],
                                 "?h:u:cg",
                                 ["help",
                                  "db-host=",
                                  "db-hostname=",
                                  "database_host=",
                                  "database_hostname=",
                                  "dbname=",
                                  "db-name=",
                                  "database_name",
                                  "db-user=",
                                  "db-username=",
                                  "database_user=",
                                  "database_username=",
                                  "database_user=",
                                  "database_username=",
                                  "db-userpass=",
                                  "db-userpassword=",
                                  "database_pass=",
                                  "database_password=",
                                  "db-password=",
                                  "email-host=",
                                  "email-hostname=",
                                  "email-addr=",
                                  "email-address=",
                                  "email-user=",
                                  "email-pass=",
                                  "email-password=",
                                  "use-tls",
                                  "no-tls",
                                  "nothing-to-hide",
                                  "i-have-nothing-to-hide",
                                  "show-pwd",
                                  "show-pass",
                                  "show-password",
                                  "hide-pwd",
                                  "hide-pass",
                                  "hide-password",
                                  "dbg",
                                  "debug"])
    except getopt.GetoptError, error:
      print(getCommandLineUsage())
      print("debug: "+ str(error))
      sys.exit(1)
    for opt, arg in opts:
        opt = opt.lower()
        if opt in ("?", "-?", "help", "--help"):
            print(getCommandLineUsage())
            sys.exit()
        elif opt in ("-h",
                     "--db-host", "--db-hostname",
                     "--database_host", "--database_hostname"):
            dbHostname    = arg
        elif opt in ("-u", "--dbname", "--db-name", "--database_name"):
            dbName        = arg
        elif opt in ("-u",
                     "--db-user", "--db-username",
                     "--database_user", "--database_username"):
            dbUserName    = arg
        elif opt in ("--db-userpass", "--db-userpassword", "--db-password",
                     "--database_pass", "--database_password",
                     "--database_userpassword", "--database_user-password"):
            dbUserPass    = arg
        elif opt in ("--email-host", "--email-hostname"):
            emailHostname = arg
        elif opt in ("--email-addr", "--email-address", "--email-user"):
            emailAddress  = arg
        elif opt in ("--email-pass", "--email-password"):
            emailPass     = arg
        elif opt in ("-c", "--use-tls"):
            useTls        = True
        elif opt in ("--no-tls", "--nothing-to-hide", "--i-have-nothing-to-hide"):
            useTls        = False
        elif opt in ("--show-pwd", "--show-pass", "--show-password"):
            hidePassInput = False
        elif opt in ("--hide-pwd", "--hide-pass", "--hide-password"):
            hidePassInput = True
        elif opt in ("-g", "--dbg", "--debug"):
            debug         = True
    
    connection = psycopg2.connect(host     = dbHostname,
                                  database = dbName,
                                  user     = dbUserName,
                                  password = dbUserPass)
    cursor = connection.cursor()
    
    imapObject = getImapObjectConnectedByAskingToUser(hostname     = emailHostname,
                                                      useTls       = useTls,
                                                      emailAddress = emailAddress,
                                                      password     = emailPass)
    result = putEmailsRawMessageContentInDataBaseOfInboxFolder(imapObject, cursor)
    if result is None or result is False:
        print("An error occured!")
    else:
        print(str(result) +" email(s) treated successfully")
    imapObject.logout()
    
    connection.commit()
    cursor.close()
    connection.close()
