# License Creative Commons 0 (like the public domain)
# https://creativecommons.org/publicdomain/zero/1.0/
# Use, study, modify and share!
# Even if you are not forced with copyleft, please respect freedom of others.
# For more informations: https://www.gnu.org/philosophy/free-sw.html
# This file is offered as-is, without any warranty.


from pyzmail_utils    import *
from email_postgresql import *


def putFileInDataBase(cursor, name, payload, mimetype):
    if name is None or name == "" or payload is None or payload == "":
        return -1
    
    cursor.execute("INSERT INTO file (name, payload, mimetype)\n"+
                   "VALUES (%s, %s, %s)\n"+
                   "ON CONFLICT DO NOTHING\n"+
                   "RETURNING id;",
                   (name, psycopg2.Binary(payload), mimetype))
    result = cursor.fetchone()
    if result is None or len(result) == 0:
        return -1
    file_id = int(result[0])
    
    if file_id is None or file_id < 0:
        cursor.execute("SELECT id FROM file WHERE\n"+
                       "name     = %s AND\n"+
                       "payload  = %s AND\n"+
                       "mimetype = %s",
                       (name, psycopg2.Binary(payload), mimetype))
        result = cursor.fetchone()
        if result is None or len(result) == 0:
            return -1
        file_id = int(result[0])
    
    return file_id

def putEmailMessageContentInDataBase(cursor, message):
    if cursor is None or message is None:
        return False
    
    message_text = getDecodedPayloadOfMessage(message.text_part)
    message_html = getDecodedPayloadOfMessage(message.html_part)
    
    if message_text is None and message_html is None:
        return False
    
    cursor.execute("INSERT INTO email_content (subject, plain_text, html)\n"+
                   "VALUES (%s, %s, %s)\n"+
                   "ON CONFLICT DO NOTHING;",
                   (message.get_subject(), message_text, message_html))
    return True

def putEmailAddressAliasesInDataBase(cursor, aliases):
    for alias in aliases:
        putEmailAddressAliasInDataBase(cursor, alias[1], alias[0])

def putEmailCorrespondentInDataBase(cursor, email_id, address, alias, label):
    if cursor is None or address is None or label is None:
        return False

    cursor.execute("INSERT INTO email_correspondent (email_id, alias_id, label)\n"+
                   "SELECT %s AS email_id,\n"+
                   "       email_address_alias.id AS alias_id,\n"+
                   "       %s AS value\n"+
                   "FROM   email_address_alias\n"+
                   "WHERE  address_id = (SELECT id FROM email_address WHERE value = %s)\n"+
                   "AND    value = %s\n"
                   "ON CONFLICT DO NOTHING;",
                   (email_id, label, address, alias))
    return True

def putEmailCorrespondentsInDataBase(cursor, email_id, aliases, label):
    for alias in aliases:
        putEmailCorrespondentInDataBase(cursor, email_id, alias[1], alias[0], label)

def putEmailCorrespondentsWithLabelOfEmailIdInDataBase(cursor, email_id, aliases, label):
    putEmailAddressAliasesInDataBase(cursor, aliases)
    putEmailCorrespondentsInDataBase(cursor, email_id, aliases, label)

def putEmailCorrespondentsOfMessageInDataBase(cursor, message):
    if cursor is None or message is None:
        return False
    
    message_text = getDecodedPayloadOfMessage(message.text_part)
    message_html = getDecodedPayloadOfMessage(message.html_part)
    
    email_id = getEmailIdOfDataBase(cursor, message_text, message_html)
    if email_id < 0:
        return False
    
    for label in ["to", "To",
                  "form", "From",
                  "reply-to", "Reply-To"
                  "cc", "Cc", "bcc", "Bcc"]:
        aliases = message.get_addresses(label)
        putEmailCorrespondentsWithLabelOfEmailIdInDataBase(cursor, email_id, aliases, label)

    return True

def putMailPartAsFileInDataBase(cursor, mailpart):
    if mailpart is None:
        return -1
    return putFileInDataBase(cursor,
                             mailpart.filename,
                             mailpart.get_payload(),
                             mailpart.type)

def putEmailAttachmentInDataBase(cursor, email_id, mailpart):
    if cursor is None or email_id < 0:
        return False
    
    file_id = putMailPartAsFileInDataBase(cursor, mailpart)
    if file_id < 0:
        return False
    
    cursor.execute("INSERT INTO email_attachment (email_id, file_id)\n"+
                   "VALUES (%s, %s)\n"+
                   "ON CONFLICT DO NOTHING;",
                   (email_id, file_id))
    return True

def putEmailAttachmentsInDataBase(cursor, message):
    if cursor is None or message is None:
        return False
    
    message_text = getDecodedPayloadOfMessage(message.text_part)
    message_html = getDecodedPayloadOfMessage(message.html_part)
    
    email_id = getEmailIdOfDataBase(cursor, message_text, message_html)
    if email_id < 0:
        return False
    
    success = True
    attachments = getAttachmentsOfMessage(message)
    for attachment in attachments:
        if not putEmailAttachmentInDataBase(cursor, email_id, attachment):
            success = False
    return success

def putEmailRawMessageContentInDataBase(cursor, message):
    message = pyzmail.PyzMessage.factory(message['BODY[]'])
    return (putEmailMessageContentInDataBase(cursor, message)
            and
            putEmailCorrespondentsOfMessageInDataBase(cursor, message)
            and
            putEmailAttachmentsInDataBase(cursor, message))

def putEmailsRawMessageContentInDataBaseWithUids(imapObject, cursor, uids):
    if imapObject is None or uids is None:
        return 0
    
    nb = 0
    for uid in uids:
        rawMessage = imapObject.fetch([uid], ["BODY[]", "FLAGS"])
        if rawMessage is not None and uid in rawMessage:
            rawMessage = rawMessage[uid]
            if putEmailRawMessageContentInDataBase(cursor, rawMessage):
                nb += 1
    return nb

def putEmailsRawMessageContentInDataBaseOfSelectedFolder(imapObject, cursor):
    uids = imapObject.search("ALL")
    return putEmailsRawMessageContentInDataBaseWithUids(imapObject, cursor, uids)

def putEmailsRawMessageContentInDataBaseOfInboxFolder(imapObject, cursor):
    if imapObject is None or not imapObject.folder_exists("INBOX"):
        return False
    
    imapObject.select_folder("INBOX", readonly=True)
    result = putEmailsRawMessageContentInDataBaseOfSelectedFolder(imapObject, cursor)
    imapObject.close_folder()
    return result
