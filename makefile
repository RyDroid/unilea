# Copying and distribution of this file, with or without modification,
# are permitted in any medium without royalty provided this notice is
# preserved.  This file is offered as-is, without any warranty.
# Names of contributors must not be used to endorse or promote products
# derived from this file without specific prior written permission.


# Folders and files
SRC_DIR=src

VIRUS_TOTAL_KEY=`cat virustotal-apikey.txt`

# Package and archive
PACKAGE=UNILEA
FILES_TO_BUILD=$(SRC_DIR)/ externals/ build.xml
FILES_TO_ARCHIVE=$(FILES_TO_BUILD) makefile \
	LICENSE_* *.md \
	.gitignore .editorconfig \
	.gitlab-ci.yml


all: default

default: build.xml
	ant

jar: build.xml
	ant jar

compile: build.xml
	ant compile

check: build.xml
	ant check


clean: \
	clean-ant clean-build clean-bin \
	clean-python clean-profiling clean-ide \
	clean-tmp clean-doc clean-archives

clean-ant: build.xml
	@ant clean

clean-build: clean-build-release clean-build-debug
	@$(RM) -rf -- build/ Build/ BUILD/ builds/ Builds/

clean-build-release:
	@$(RM) -rf -- release/ Release/

clean-build-debug:
	@$(RM) -rf -- dbg/ DBG/ debug/ Debug/

clean-bin:
	@$(RM) -rf -- \
		*.o *.a *.so *.ko *.lo *.dll *.out \
		bin/ BIN/ binaries/

clean-python:
	@$(RM) -rf -- *.pyc __pycache__

clean-profiling:
	@$(RM) -f -- callgrind.out.*

clean-ide: clean-qt-creator clean-codeblocks

clean-qt-creator:
	@$(RM) -f -- qmake_makefile *.pro.user

clean-codeblocks:
	@$(RM) -f -- *.cbp *.CBP

clean-tmp:
	@$(RM) -rf -- \
		*~ .\#* \#* \
		*.swp *.swap *.SWP *.SWAP \
		*.bak *.backup *.BAK *.BACKUP \
		*.sav *.save *.SAV *.SAVE \
		*.autosav *.autosave \
		*.log *.log.* error_log* log/ logs/ \
		.cache/ .thumbnails/

clean-doc:
	@$(RM) -rf -- \
		doc/ Doc/ docs/ Docs/ \
		documentation/ Documentation/

clean-latex:
	@$(RM) -f -- \
		*.pdf *.dvi *.ps \
		*.PDF *.DVI *.PS \
		*.acn *.aux *.bcf *.cut *.fls *.glo *.ist *.lof *.nav \
		*.run.xml *.snm *.toc *.vrb *.vrm *.xdy \
		*.fdb_latexmk *-converted-to.* \
		*.synctex *.synctex.gz *.synctex.bz2 *.synctex.xz \
		*.synctex.zip *.synctex.rar

clean-tests: clean-tests-files

clean-tests-files:
	@$(RM) -rf -- \
		a b c a.* b.* c.* \
		test.c tests.c \
		test.cc tests.cc test.cpp tests.cpp \
		test.java tests.java \
		test.py tests.py \
		junit* report* TEST* \
		*arg0* *arg1* *arg2* \
		a.csv b.csv c.csv

clean-archives:
	@$(RM) -rf -- \
		*.deb *.rpm *.exe *.msi *.dmg *.apk *.ipa \
		*.DEB *.RPM *.EXE *.MSI *.DMG *.APK *.IPA \
		*.tar.* *.tgz *.gz *.bz2 *.lz *.lzma *.xz \
		*.TAR.* *.TGZ *.GZ *.BZ2 *.LZ *.LZMA *.XZ \
		*.zip *.7z *.rar *.jar \
		*.ZIP *.7Z *.RAR *.JAR \
		*.iso *.ciso *.img *.gcz *.wbfs \
		*.ISO *.CISO *.IMG *.GCZ *.WBFS

clean-git:
	git clean -fdx


run-virustotal-file: build.xml virustotal-apikey.txt
	ant -Dkey=$(VIRUS_TOTAL_KEY) run-virustotal-file

run-virustotal-file-verbose: build.xml virustotal-apikey.txt
	ant -Dkey=$(VIRUS_TOTAL_KEY) run-virustotal-file-verbose

run-email-find-links-export: build.xml
	ant -Darg0=links-of-email-without-links-in-db.csv run-email-find-links-export

run-email-links-export: build.xml
	ant -Darg0=email-links.csv run-email-links-export

run-email-links-import: build.xml email-links-to-import.csv
	ant -Darg0=email-links-to-import.csv run-email-links-import

run-webpage-links-import: build.xml webpage-links-to-import.csv
	ant -Darg0=webpage-links-to-import.csv run-webpage-links-import

run-virustotal-webpage: build.xml virustotal-apikey.txt
	ant -Dkey=$(VIRUS_TOTAL_KEY) run-virustotal-webpage

run-virustotal-webpage-verbose: build.xml virustotal-apikey.txt
	ant -Dkey=$(VIRUS_TOTAL_KEY) run-virustotal-webpage-verbose

run-score-virustotal-webpage: build.xml
	ant run-score-virustotal-webpage

run-score-virustotal-email: build.xml
	ant run-score-virustotal-email

run-print-score-int-webpage: build.xml
	ant run-print-score-int-webpage


archives: zip tar-gz tar-bz2 tar-xz 7z

dist: default-archive

default-archive: tar-xz

zip: $(PACKAGE).zip

$(PACKAGE).zip: $(FILES_TO_ARCHIVE)
	zip $(PACKAGE).zip -r -- $(FILES_TO_ARCHIVE)

tar-gz: $(PACKAGE).tar.gz

$(PACKAGE).tar.gz: $(FILES_TO_ARCHIVE)
	tar -zcvf $(PACKAGE).tar.gz -- $(FILES_TO_ARCHIVE)

tar-bz2: $(PACKAGE).tar.bz2

$(PACKAGE).tar.bz2: $(FILES_TO_ARCHIVE)
	tar -jcvf $(PACKAGE).tar.bz2 -- $(FILES_TO_ARCHIVE)

tar-xz: $(PACKAGE).tar.xz

$(PACKAGE).tar.xz: $(FILES_TO_ARCHIVE)
	tar -cJvf $(PACKAGE).tar.xz -- $(FILES_TO_ARCHIVE)

7z: $(PACKAGE).7z

$(PACKAGE).7z: $(FILES_TO_ARCHIVE)
	7z a -t7z $(PACKAGE).7z $(FILES_TO_ARCHIVE)
