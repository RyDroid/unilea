<?xml version="1.0" encoding="UTF-8"?>
<project name="UNILEA" default="all">
  <!--
      License Creative Commons 0 (like the public domain)
      https://creativecommons.org/publicdomain/zero/1.0/
      
      Use, study, modify and share!
      Even if you are not forced with copyleft, please respect freedom of others.
      For more information: https://www.gnu.org/philosophy/free-sw.html
      
      This file is offered as-is, without any warranty.
      Names of contributors must not be used to endorse or promote products
      derived from this file without specific prior written permission.
  -->

  
  <property name="projectName" value="UNILEA" />
  <description>
    The aim of this project is to create an analyzer of emails to find potential security threats.
    It is free/libre software.
  </description>
  
  <property name="jdk.version" value="1.7" />
  <property name="java.encoding" value="utf-8" />
  
  <property name="src.dir"     value="src/java" />
  <property name="main.dir"    value="${src.dir}/main" />
  <property name="test.dir"    value="${src.dir}/test" />
  <property name="build.dir"   value="build/java" />
  <property name="classes.dir" value="${build.dir}/classes" />
  <property name="jar.dir"     value="${build.dir}/jar" />
  <property name="lib.dir"     value="/usr/share/java" />
  <property name="lib-ext.dir" value="externals/java" />
  <property name="package.dir" value="br/unicamp/ic/lasca/unilea" />
  
  <path id="classpath-main">
    <fileset dir="${lib.dir}" includes="commons-csv.jar" />
    <fileset dir="${lib.dir}" includes="postgresql.jar" />
    <fileset dir="${lib.dir}" includes="jsoup.jar" />
    <fileset dir="${lib-ext.dir}" includes="*.jar" />
  </path>
  
  <path id="classpath-tests">
    <path refid="classpath-main"/>
    <fileset dir="${lib.dir}" includes="junit4.jar" />
    <pathelement location="${classes.dir}" />
  </path>
  
  
  <target name="clean">
    <delete dir="${build.dir}" />
    <delete dir="release" />
    <delete dir="debug" />
    <delete dir="dbg" />
    <delete dir="bin" />
  </target>
  
  
  <target name="all"
	  depends="compile,jar" />
  
  
  <target name="compile-main">
    <mkdir dir="${classes.dir}" />
    <javac srcdir="${main.dir}"
	   destdir="${classes.dir}"
	   classpathref="classpath-main"
	   includeantruntime="false"
	   encoding="${java.encoding}" />
  </target>
  
  <target name="compile-tests"
	  depends="compile-main">
    <mkdir dir="${classes.dir}" />
    <javac srcdir="${test.dir}"
	   destdir="${classes.dir}"
	   classpathref="classpath-tests"
	   includeantruntime="false"
	   encoding="${java.encoding}" />
  </target>
  
  <target name="compile"
	  depends="compile-tests" />
  
  
  <target name="jar-file-import" depends="compile-main">
    <mkdir dir="${jar.dir}" />
    
    <jar destfile="${jar.dir}/FilesAdderToDb.jar"
	 basedir="${classes.dir}">
      <manifest>
        <attribute name="Main-Class"
		   value="FilesAdderToDb" />
      </manifest>
    </jar>
  </target>
  
  <target name="jar-virustotal-file" depends="compile-main">
    <mkdir dir="${jar.dir}" />
    
    <jar destfile="${jar.dir}/VirusTotalFileAnalyze.jar"
	 basedir="${classes.dir}">
      <manifest>
        <attribute name="Main-Class"
		   value="VirusTotalFileAnalyze" />
      </manifest>
    </jar>
  </target>
  
  <target name="jar-email-find-links-export" depends="compile-main">
    <mkdir dir="${jar.dir}" />
    
    <jar destfile="${jar.dir}/EmailInDbWithoutLinksToCsvLinks.jar"
	 basedir="${classes.dir}">
      <manifest>
        <attribute name="Main-Class"
		   value="EmailInDbWithoutLinksToCsvLinks" />
      </manifest>
    </jar>
  </target>
  
  <target name="jar-email-links-export" depends="compile-main">
    <mkdir dir="${jar.dir}" />
    
    <jar destfile="${jar.dir}/EmailLinksDbToCsv.jar"
	 basedir="${classes.dir}">
      <manifest>
        <attribute name="Main-Class"
		   value="EmailLinksDbToCsv" />
      </manifest>
    </jar>
  </target>
  
  <target name="jar-email-links-import" depends="compile-main">
    <mkdir dir="${jar.dir}" />
    
    <jar destfile="${jar.dir}/EmailLinksCsvToDb.jar"
	 basedir="${classes.dir}">
      <manifest>
        <attribute name="Main-Class"
		   value="EmailLinksCsvToDb" />
      </manifest>
    </jar>
  </target>
  
  <target name="jar-links-import" depends="compile-main">
    <mkdir dir="${jar.dir}" />
    
    <jar destfile="${jar.dir}/LinkCheckerCsvToDb.jar"
	 basedir="${classes.dir}">
      <manifest>
        <attribute name="Main-Class"
		   value="LinkCheckerCsvToDb" />
      </manifest>
    </jar>
  </target>
  
  <target name="jar-virustotal-webpage" depends="compile-main">
    <mkdir dir="${jar.dir}" />
    
    <jar destfile="${jar.dir}/VirusTotalWebPageAnalyze.jar"
	 basedir="${classes.dir}">
      <manifest>
        <attribute name="Main-Class"
		   value="VirusTotalWebPageAnalyze" />
      </manifest>
    </jar>
  </target>
  
  <target name="jar-score-virustotal-webpage" depends="compile-main">
    <mkdir dir="${jar.dir}" />
    
    <jar destfile="${jar.dir}/VirusTotalWebPageAnalyzeSimpleScoreManager.jar"
	 basedir="${classes.dir}">
      <manifest>
        <attribute name="Main-Class"
		   value="VirusTotalWebPageAnalyzeSimpleScoreManager" />
      </manifest>
    </jar>
  </target>
  
  <target name="jar-score-virustotal-email" depends="compile-main">
    <mkdir dir="${jar.dir}" />
    
    <jar destfile="${jar.dir}/VirusTotalEmailAnalyzeSimpleScoreManager.jar"
	 basedir="${classes.dir}">
      <manifest>
        <attribute name="Main-Class"
		   value="VirusTotalEmailAnalyzeSimpleScoreManager" />
      </manifest>
    </jar>
  </target>
  
  <target name="jar-print-score-int-webpage" depends="compile-main">
    <mkdir dir="${jar.dir}" />
    
    <jar destfile="${jar.dir}/WebPageScoreIntegerPrinter.jar"
	 basedir="${classes.dir}">
      <manifest>
        <attribute name="Main-Class"
		   value="WebPageScoreIntegerPrinter" />
      </manifest>
    </jar>
  </target>
  
  <target name="jar-file"
	  depends="jar-file-import" />
  
  <target name="jar-email-links-export-all"
	  depends="jar-email-find-links-export,jar-email-links-export" />
  
  <target name="jar-email"
	  depends="jar-email-links-export-all,jar-email-links-import" />
  
  <target name="jar-virustotal"
	  depends="jar-virustotal-file,jar-virustotal-webpage,jar-score-virustotal" />
  
  <target name="jar-score-virustotal"
	  depends="jar-score-virustotal-webpage,jar-score-virustotal-email" />

  <target name="jar-print-score"
	  depends="jar-print-score-int-webpage" />

  <target name="jar-score"
	  depends="jar-score-virustotal,jar-print-score" />
  
  <target name="jar"
	  depends="jar-file,jar-email,jar-links-import,jar-virustotal,jar-score" />
  
  
  <target name="run-file-import"
	  depends="jar-file-import">
    <java classname="${package.dir}/file/FilesAdderToDb"
	  fork="true">
      <classpath>
        <path refid="classpath-main" />
        <path location="${jar.dir}/FilesAdderToDb.jar" />
      </classpath>
      
      <arg value="${arg0}" />
    </java>
  </target>
  
  <target name="run-virustotal-file"
	  depends="jar-virustotal-file">
    <java classname="${package.dir}/file/analyze/virustotal/VirusTotalFileAnalyze"
	  fork="true">
      <classpath>
        <path refid="classpath-main" />
        <path location="${jar.dir}/VirusTotalFileAnalyze.jar" />
      </classpath>
      
      <arg value="--quiet" />
      <arg value="${key}" />
    </java>
  </target>
  
  <target name="run-virustotal-file-verbose"
	  depends="jar-virustotal-file">
    <java classname="${package.dir}/file/analyze/virustotal/VirusTotalFileAnalyze"
	  fork="true">
      <classpath>
        <path refid="classpath-main" />
        <path location="${jar.dir}/VirusTotalFileAnalyze.jar" />
      </classpath>
      
      <arg value="--verbose" />
      <arg value="${key}" />
    </java>
  </target>
  
  <target name="run-virustotal-file-nb-unanalized"
	  depends="jar-virustotal-file">
    <java classname="${package.dir}/file/analyze/virustotal/VirusTotalFileAnalyze"
	  fork="true">
      <classpath>
        <path refid="classpath-main" />
        <path location="${jar.dir}/VirusTotalFileAnalyze.jar" />
      </classpath>
      
      <arg value="--nb-unanalized" />
      <arg value="${key}" />
    </java>
  </target>
  
  <target name="run-email-find-links-export"
	  depends="jar-email-find-links-export">
    <java classname="${package.dir}/email/EmailInDbWithoutLinksToCsvLinks"
	  fork="true">
      <classpath>
        <path refid="classpath-main" />
        <path location="${jar.dir}/EmailInDbWithoutLinksToCsvLinks.jar" />
      </classpath>
      
      <arg value="${arg0}" />
    </java>
  </target>
  
  <target name="run-email-links-export"
	  depends="jar-email-links-export">
    <java classname="${package.dir}/email/EmailLinksDbToCsv"
	  fork="true">
      <classpath>
        <path refid="classpath-main" />
        <path location="${jar.dir}/EmailLinksDbToCsv.jar" />
      </classpath>
      
      <arg value="${arg0}" />
    </java>
  </target>
  
  <target name="run-email-links-import"
	  depends="jar-email-links-import">
    <java classname="${package.dir}/email/EmailLinksCsvToDb" fork="true">
      <classpath>
        <path refid="classpath-main" />
        <path location="${jar.dir}/EmailLinksCsvToDb.jar" />
      </classpath>
      
      <arg value="${arg0}" />
    </java>
  </target>
  
  <target name="run-webpage-links-import"
	  depends="jar-links-import">
    <java classname="${package.dir}/webpage/LinkCheckerCsvToDb" fork="true">
      <classpath>
        <path refid="classpath-main" />
        <path location="${jar.dir}/LinkCheckerCsvToDb.jar" />
      </classpath>
      
      <arg value="${arg0}" />
    </java>
  </target>
  
  <target name="run-virustotal-webpage"
	  depends="jar-virustotal-webpage">
    <java classname="${package.dir}/webpage/analyze/virustotal/VirusTotalWebPageAnalyze"
	  fork="true">
      <classpath>
        <path refid="classpath-main" />
        <path location="${jar.dir}/VirusTotalWebPageAnalyze.jar" />
      </classpath>
      
      <arg value="--quiet" />
      <arg value="${key}" />
    </java>
  </target>
  
  <target name="run-virustotal-webpage-verbose"
	  depends="jar-virustotal-webpage">
    <java classname="${package.dir}/webpage/analyze/virustotal/VirusTotalWebPageAnalyze"
	  fork="true">
      <classpath>
        <path refid="classpath-main" />
        <path location="${jar.dir}/VirusTotalWebPageAnalyze.jar" />
      </classpath>
      
      <arg value="--verbose" />
      <arg value="${key}" />
    </java>
  </target>
  
  <target name="run-virustotal-webpage-nb-unanalized"
	  depends="jar-virustotal-webpage">
    <java classname="${package.dir}/webpage/analyze/virustotal/VirusTotalWebPageAnalyze"
	  fork="true">
      <classpath>
        <path refid="classpath-main" />
        <path location="${jar.dir}/VirusTotalWebPageAnalyze.jar" />
      </classpath>
      
      <arg value="--nb-unanalized" />
      <arg value="${key}" />
    </java>
  </target>
  
  <target name="run-score-virustotal-webpage"
	  depends="jar-score-virustotal-webpage">
    <java classname="${package.dir}/webpage/analyze/virustotal/VirusTotalWebPageAnalyzeSimpleScoreManager"
	  fork="true">
      <classpath>
        <path refid="classpath-main" />
        <path location="${jar.dir}/VirusTotalWebPageAnalyzeSimpleScoreManager.jar" />
      </classpath>
    </java>
  </target>
  
  <target name="run-score-virustotal-email"
	  depends="jar-score-virustotal-email">
    <java classname="${package.dir}/email/score/VirusTotalEmailAnalyzeSimpleScoreManager"
	  fork="true">
      <classpath>
        <path refid="classpath-main" />
        <path location="${jar.dir}/VirusTotalEmailAnalyzeSimpleScoreManager.jar" />
      </classpath>
    </java>
  </target>
  
  <target name="run-print-score-int-webpage"
	  depends="jar-print-score-int-webpage">
    <java classname="${package.dir}/webpage/analyze/WebPageScoreIntegerPrinter"
	  fork="true">
      <classpath>
        <path refid="classpath-main" />
        <path location="${jar.dir}/WebPageScoreIntegerPrinter.jar" />
      </classpath>
    </java>
  </target>

  
  <target name="check"
	  depends="test" />
  
  <target name="test"
	  depends="junit" />
  
  <target name="junit" depends="compile-tests">
    <junit printsummary ="no"
	   haltonerror  ="yes"
	   haltonfailure="yes">
      <formatter type="plain" usefile="false" />
      <formatter type="plain" />
      <formatter type="xml" />
      
      <classpath>
        <path refid="classpath-tests" />
      </classpath>
      
      <batchtest fork="yes">
	<fileset dir="${test.dir}">
	  <include name="**/*Tests.java" />
	</fileset>
      </batchtest>
    </junit>
  </target>
  
  
  <target name="archives">
    <exec executable="make">
      <arg value="archives" />
    </exec>
  </target>
  
  <target name="dist">
    <exec executable="make">
      <arg value="dist" />
    </exec>
  </target>
  
  <target name="default-archive">
    <exec executable="make">
      <arg value="default-archive" />
    </exec>
  </target>
  
  <target name="zip">
    <exec executable="make">
      <arg value="zip" />
    </exec>
  </target>
  
  <target name="tar-gz">
    <exec executable="make">
      <arg value="tar-gz" />
    </exec>
  </target>
  
  <target name="tar-bz2">
    <exec executable="make">
      <arg value="tar-bz2" />
    </exec>
  </target>
  
  <target name="tar-xz">
    <exec executable="make">
      <arg value="tar-xz" />
    </exec>
  </target>
  
  <target name="7z">
    <exec executable="make">
      <arg value="7z" />
    </exec>
  </target>
</project>
